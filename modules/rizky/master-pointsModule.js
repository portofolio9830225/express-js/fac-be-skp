const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const MasterPointsModel = db.master_points;
const helper = require("../../common/helper");
const { readFileSync, unlinkSync } = require("fs");
const { ObjectId } = require("mongodb");

const logSlack = require("../../helpers/slack");

const createSchema = Joi.object({
	name: Joi.string().required(),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
const idSchema = Joi.object({
	_id: Joi.string().required(),
});

function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

async function createMasterPoints(payload) {
	try {
		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		payload.is_active = true;

		const masterPoints = await new MasterPointsModel(payload).save();

		return { data: masterPoints, status: "Success master points has created" };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function readAllMasterPoints(query) {
	try {
		let condition = {
			is_active: true,
		};

		// sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "created_at";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					name: -1,
			  };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;
		const masterPoints = await MasterPointsModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -created_at -updated_at -is_deleted")
			.lean();

		const masterPointsAll = await MasterPointsModel.find(condition).lean();
		if (helper.isEmptyObject(masterPoints)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		masterPoints.forEach(async (_masterPoints, index) => {
			let _master_point_result = {
				name: _masterPoints.name,
				_id: _masterPoints._id,
			};
			result.push(_master_point_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(masterPointsAll).length / sizePerPage),
			countData: Object.keys(masterPointsAll).length,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function deleteMasterPoints(payload) {
	try {
		const validate = validateIdSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };
		const update = {
			is_active: false,
		};
		let masterPointsDelete = await MasterPointsModel.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(masterPointsDelete)) {
			return error.errorReturn({ message: "Master Points not found" });
		}

		return { data: masterPointsDelete, status: "Data has been deleted successfully" };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function updateMasterPoints(payload) {
	try {
		const filter = { _id: ObjectId(payload._id) };
		let update = payload;
		let masterPoints = await MasterPointsModel.findOneAndUpdate(filter, payload, {
			useFindAndModify: false,
			new: true,
		});

		if (helper.isEmptyObject(masterPoints)) {
			return error.errorReturn({ message: "Master Point not found" });
		}

		return {
			data: masterPoints,
			status: "Data has been updated successfully",
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function list(query) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		const sort = { name: 1 };

		const result = await MasterPointsModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(result)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let resultfinal = [];

		result.forEach(async (_e, index) => {
			let _e_result = {
				value: _e.name,
				text: _e.name,
				label: _e.name,
			};
			resultfinal.push(_e_result);
		});

		return {
			countData: Object.keys(result).length,
			foundData: resultfinal,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	createMasterPoints,
	readAllMasterPoints,
	deleteMasterPoints,
	updateMasterPoints,
	list,
};
