const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const moment = require("moment");
const ParametersModel = db.parameter;
const helper = require("../../common/helper");
const { readFileSync, unlinkSync } = require("fs");
const { ObjectId } = require("mongodb");
const { format } = require("path");

const logSlack = require("../../helpers/slack");

async function listYear() {
	try {
		let condition = {
			is_active: true,
		};

		const listYear = await ParametersModel.distinct("year", condition).lean();

		if (helper.isEmptyArray(listYear)) {
			return {
				foundData: [],
				countData: 0,
			};
		}

		let result = [];

		listYear.forEach(async (e, index) => {
			let _year_result = {
				value: e,
				text: e,
				label: e,
			};
			result.push(_year_result);
		});

		return {
			countData: Object.keys(listYear).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	listYear,
};
