const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const SkpModel = db.skp;
const moment = require("moment");
const helper = require("../../common/helper");
const { readFileSync, unlinkSync } = require("fs");
const { ObjectId } = require("mongodb");

const logSlack = require("../../helpers/slack");

async function create(payload_params, currentUser) {
	try {
		let payload = payload_params;

		const insert = {
			is_active: true,
		};

		insert.skp_number = payload.number_skp.toUpperCase();
		insert.program_name = payload.program_name;
		insert.mechanism = payload.mechanism;
		insert.program_start = new Date(payload.date_start).toISOString();
		insert.program_end = new Date(payload.date_end).toISOString();
		insert.amount = payload.amount;
		insert.account_name = payload.account_name;
		insert.account_code = payload.account_code;
		insert.created_at = new Date();
		let product = [];
		payload.product.forEach(async (e, i) => {
			let _result = {
				product_code: e.value,
				product_name: e.text,
			};
			product.push(_result);
		});
		insert.product = product;
		if (payload.optional_parameter) {
			insert.optional_parameter = payload.optional_parameter;
		}

		const result = await new SkpModel(insert).save();

		return { data: result, status: "success " };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload_params);
		return error.errorReturn({ message: err.response.data });
	}
}

async function updateSkp(payload_params) {
	try {
		let payload = payload_params;
		const filter = { _id: ObjectId(payload._id) };

		const insert = {
			is_active: true,
		};

		insert.skp_number = payload.number_skp.toUpperCase();
		insert.program_name = payload.program_name;
		insert.mechanism = payload.mechanism;
		insert.program_start = new Date(payload.date_start).toISOString();
		insert.program_end = new Date(payload.date_end).toISOString();
		insert.amount = payload.amount;
		insert.account_name = payload.account_name;
		insert.account_code = payload.account_code;
		let product = [];
		payload.product.forEach(async (e, i) => {
			let _result = {
				product_code: e.value,
				product_name: e.text,
			};
			product.push(_result);
		});
		insert.product = product;
		insert.optional_parameter = payload.optional_parameter;

		let result = await SkpModel.findOneAndUpdate(filter, insert, {
			useFindAndModify: false,
			new: true,
		});

		if (helper.isEmptyObject(result)) {
			return error.errorReturn({ message: "SKP not found" });
		}

		return { data: result, status: "Data has been updated successfully" };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload_params);
		return error.errorReturn({ message: err.response.data });
	}
}

async function readTable(query) {
	try {
		let condition = {
			is_active: true,
		};

		if (query?.store_code) {
			condition.account_code = query.store_code;
		}

		if (query?.datee) {
			condition.program_start = {
				$gte: new Date(`01/01/${query.datee}`),
				$lte: new Date(`12/31/${query.datee}`),
			};
		}

		if (query?.skp_number) {
			condition.skp_number = { $regex: ".*" + query.skp_number.toUpperCase() + ".*" };
		}

		// sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "created_at";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					created_at: -1,
			  };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;
		const Skp = await SkpModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -updated_at -is_deleted")
			.lean();

		const allData = await SkpModel.find(condition).lean();
		if (helper.isEmptyObject(Skp)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		Skp.forEach(async (_skp, index) => {
			let product = [];
			let _skp_result = {
				name: _skp.program_name,
				skp_number: _skp.skp_number,
				mechanism: _skp.mechanism,
				program_start: _skp.program_start,
				program_end: _skp.program_end,
				amount: _skp.amount,
				account: {
					text: _skp.account_name,
					value: _skp.account_code,
					label: _skp.account_name,
				},
				area: _skp.optional_parameter?.area_program,
				average_prev_year: _skp.optional_parameter?.average_prev_year,
				budget_disc: _skp.optional_parameter?.budget_disc,
				budget_promo: _skp.optional_parameter?.budget_promo,
				channel: _skp.optional_parameter?.channel,
				claim_attachment: _skp.optional_parameter?.claim_attachment,
				claim_period: _skp.optional_parameter?.claim_period,
				estimated_sales: _skp.optional_parameter?.estimated_sales,
				growth: _skp.optional_parameter?.growth,
				listing_cost: _skp.optional_parameter?.listing_cost,
				rent_cost: _skp.optional_parameter?.rent_cost,
				rent_location: _skp.optional_parameter?.rent_location,
				_id: _skp._id,
			};
			if (Number(moment(_skp.created_at).format("YYYY")) === Number(moment().format("YYYY"))) {
				if (Number(moment(_skp.created_at).format("MM")) < Number(moment().format("MM"))) {
					_skp_result.isDisable = true;
				} else {
					_skp_result.isDisable = false;
				}
			} else {
				_skp_result.isDisable = true;
			}
			_skp.product.forEach(async (e, i) => {
				let _skp_result = {
					text: e.product_name,
					value: e.product_code,
					label: e.product_name,
				};
				product.push(_skp_result);
			});
			_skp_result.product = product;
			result.push(_skp_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(allData).length / sizePerPage),
			countData: Object.keys(allData).length,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function checkSkpNumber(payload) {
	try {
		let condition = {
			is_active: true,
			skp_number: payload.skp_number.toUpperCase(),
		};

		const skp = await SkpModel.findOne(condition).lean();
		if (helper.isEmptyObject(skp)) {
			return {
				data: false,
			};
		}
		return {
			data: true,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function deleteSkp(payload) {
	try {
		const filter = { _id: payload._id };
		const update = {
			is_active: false,
			// belum date range
		};
		let skpDelete = await SkpModel.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(skpDelete)) {
			return error.errorReturn({ message: "TT Poin not found" });
		}

		return { data: skpDelete, status: "Data has been deleted successfully" };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function list() {
	try {
		let condition = {
			is_active: true,
		};

		const sort = { skp_number: 1 };

		const skp = await SkpModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(skp)) {
			return {
				foundData: [],
				countData: 0,
			};
		}
		let result = [];

		skp.forEach(async (_skp, index) => {
			let _skp_result = {
				value: _skp.skp_number,
				text: _skp.program_name,
				label: `${_skp.skp_number} - ${_skp.account_name} - ${_skp.program_name}`,
			};
			result.push(_skp_result);
		});

		return {
			countData: Object.keys(skp).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	create,
	readTable,
	updateSkp,
	checkSkpNumber,
	deleteSkp,
	list,
};
