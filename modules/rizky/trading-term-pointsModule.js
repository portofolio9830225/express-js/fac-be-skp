const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const TradingTermPointsModel = db.trading_term_points;
const moment = require("moment");
const helper = require("../../common/helper");
const { readFileSync, unlinkSync } = require("fs");
const { ObjectId } = require("mongodb");
const debug = require("debug")("backend-skp-tt:ttModule");

const logSlack = require("../../helpers/slack");
const { triggerAsyncId } = require("async_hooks");

const createSchema = Joi.object({
	account_name: Joi.string().required(),
	account_code: Joi.string().required(),
	year: Joi.number().required(),
	year_iso: Joi.string().required(),
	is_active: Joi.bool().required(),
	point: Joi.array().items(
		Joi.object({
			name: Joi.string().required(),
			value: Joi.string().required(),
			type: Joi.string().required(),
		})
	),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}

const idSchema = Joi.object({
	_id: Joi.string().required(),
});

function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

async function create(payload_params, currentUser) {
	try {
		let payload = payload_params;

		const insert = {
			is_active: true,
		};

		insert.account_name = payload.account;
		insert.account_code = payload.account;
		insert.year = payload.yr.toString();
		insert.year_iso = new Date(`01/01/${payload.yr.toString()}`).toISOString();
		insert.point = payload.point;

		const validate = validateCreateSchema(insert);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		insert.created_at = new Date();

		const result = await new TradingTermPointsModel(insert).save();
		return { data: result, status: "success " };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload_params);
		return error.errorReturn({ message: err.response.data });
	}
}

async function update(payload_params) {
	try {
		let payload = payload_params;
		const filter = { _id: ObjectId(payload._id) };

		const insert = {
			is_active: true,
		};

		insert.account_name = payload.account;
		insert.account_code = payload.account;
		insert.point = payload.point;

		let result = await TradingTermPointsModel.findOneAndUpdate(filter, insert, {
			useFindAndModify: false,
			new: true,
		});

		if (helper.isEmptyObject(result)) {
			return error.errorReturn({ message: "TT Poin not found" });
		}

		return { data: result, status: "Data has been updated successfully" };
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload_params);
		return error.errorReturn({ message: err.response.data });
	}
}

async function readTable(query) {
	try {
		console.log(query);
		let condition = {
			is_active: true,
		};

		if (query?.store_code) {
			condition.account_code = query.store_code;
		}

		if (query?.datee) {
			condition.year = query.datee;
		}

		debug(condition);

		// sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "created_at";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					created_at: -1,
			  };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;
		const masterPoints = await TradingTermPointsModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -created_at -updated_at -is_deleted")
			.lean();

		const allData = await TradingTermPointsModel.find(condition).lean();
		if (helper.isEmptyObject(masterPoints)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		masterPoints.forEach(async (_tradingTermPoin, index) => {
			let _trading_term_poin_result = {
				name: _tradingTermPoin.account_name,
				year: _tradingTermPoin.year,
				point: _tradingTermPoin.point,
				_id: _tradingTermPoin._id,
			};
			result.push(_trading_term_poin_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(allData).length / sizePerPage),
			countData: Object.keys(allData).length,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function checkStore(payload) {
	try {
		let condition = {
			is_active: true,
			year: payload.yr.value.toString() || moment().format("YYYY"),
		};

		condition.account_code = payload.store.value;
		condition.year = payload.yr.value.toString();

		const trading_term_points = await TradingTermPointsModel.findOne(condition).lean();
		if (helper.isEmptyObject(trading_term_points)) {
			return {
				data: true,
			};
		}
		return {
			data: false,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function deleteTTPoin(payload) {
	try {
		const validate = validateIdSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };
		const update = {
			is_active: false,
		};
		let masterPointsDelete = await TradingTermPointsModel.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(masterPointsDelete)) {
			return error.errorReturn({ message: "TT Poin not found" });
		}

		return {
			data: masterPointsDelete,
			status: "Data has been deleted successfully",
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function listSelect(payload) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		debug("listSelect");

		const resultAll = await TradingTermPointsModel.find(condition).lean();
		if (helper.isEmptyObject(resultAll)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let result = [];

		debug(resultAll);
		resultAll.forEach(async e => {
			let _temp_result = {
				value: e._id,
				text: e.account_code,
				label: `${e.account_name} (${e.year})`,
			};
			_temp_result.point = [];
			e.point.forEach((el, i) => {
				let _temp_point = {
					value: i,
					text: el.name,
					label: `${el.name} - ${el.value} ${el.type === "nominal" ? "IDR" : "%"}`,
				};
				_temp_result.point.push(_temp_point);
			});
			result.push(_temp_result);
		});

		return {
			countData: Object.keys(resultAll).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function listYear(payload) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		const resultAll = await TradingTermPointsModel.find(condition).distinct("year").lean();
		if (helper.isEmptyObject(resultAll)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let result = [];

		debug(resultAll);
		resultAll.forEach(async e => {
			let _temp_result = {
				value: e,
				text: e,
			};
			result.push(_temp_result);
		});

		return {
			countData: Object.keys(resultAll).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	create,
	readTable,
	update,
	checkStore,
	deleteTTPoin,
	listSelect,
	listYear,
};
