const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-skp-tt:master-nka-module");
const helper = require("../../common/helper");
const db = require("../../models");
const ProductModel = db.products;

const logSlack = require("../../helpers/slack");

async function list(query) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		const sort = { product_name: 1 };

		const products = await ProductModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(products)) {
			return {
				foundData: [],
				countData: 0,
			};
		}
		let result = [];

		products.forEach(async (_products, index) => {
			let _products_result = {
				value: _products.product_code,
				text: _products.product_name,
				label: _products.product_name,
			};
			result.push(_products_result);
		});

		return {
			countData: Object.keys(products).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	list,
};
