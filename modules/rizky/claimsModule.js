const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const DummyClaimsModel = db.dummy_claim;
const DummyParametersModel = db.dummy_parameter;
const ClaimsModel = db.claims;
const Claim_F = db.claim_F;
const SkpModel = db.skp;
const BrandModel = db.brand;
const moment = require("moment");
const TradingTermPointsModel = db.trading_term_points;
const helper = require("../../common/helper");
const { readFileSync, unlinkSync } = require("fs");
const { ObjectId } = require("mongodb");
const { format } = require("path");
const generatedRealisationModule = require("../generatedRealisationModule");
const Parameter = db.parameter;
const mongoose = require("mongoose");

const debug = require("debug")("backend-skp-tt:claimModule");

const logSlack = require("../../helpers/slack");

const xl = require("excel4node");

const createSchema = Joi.object({
	periode_claim: Joi.string().required(),
	amount: Joi.number().required(),
	remark: Joi.string().required(),
	type: Joi.string().required(),
	code: Joi.required(),
	account_name: Joi.string(),
	promo_type: Joi.string(),
	distributor: Joi.string().allow(null).allow(""),
	invoice_number: Joi.string().allow(null).allow(""),
	branch: Joi.string().allow(null).allow(""),
	brand: Joi.string().allow(null).allow(""),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
const idSchema = Joi.object({
	_id: Joi.string().required(),
});

function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

async function create(payload) {
	try {
		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const create = {
			is_active: true,
			periode_claim: `${payload.periode_claim}-01`,
			amount: payload.amount,
			remark: payload.remark,
			type: payload.type,
			created_at: new Date(),
			distributor: payload.distributor,
			invoice_number: payload.invoice_number,
			branch: payload.branch,
			source: "input",
		};

		if (payload.type === "skp") {
			const filter = {
				skp_number: payload.code,
				is_active: true,
			};
			const skp = await SkpModel.findOne(filter).lean();
			//check skp year
			if (helper.isEmptyObject(skp)) {
				return error.errorReturn({
					message: "SKP not found",
				});
			}

			if (moment(skp.program_start).format("YYYY") !== moment(payload.periode_claim).format("YYYY")) {
				return error.errorReturn({
					message: "claim period must be " + moment(skp.program_start).format("YYYY"),
				});
			}

			create.skp = skp;
			create.promo_type = payload.promo_type;
			create.account_name = skp.account_name;
			create.account_code = skp.account_code;
		} else {
			const year = moment(payload.periode_claim + "-01").format("YYYY");
			const month = moment(payload.periode_claim + "-01").format("M");

			const filter = {
				_id: payload.account_name,
				year: year,
				is_active: true,
			};
			const ttp = await TradingTermPointsModel.findOne(filter).lean();

			if (helper.isEmptyObject(ttp)) {
				return error.errorReturn({
					message: "trading term point not found in the entered claim period",
				});
			}

			// Search Persen
			let pointSelect = {};
			ttp.point.forEach((el, i) => {
				if (i == payload.code) {
					pointSelect = el;
				}
			});

			if (Object.keys(pointSelect).length != 0) {
				// Search account_code dari Parameter
				const param = await Parameter.findOne({
					is_active: true,
					type: "sell_in",
					year: year,
					store_code: ttp.account_code,
				});

				if (helper.isEmptyObject(param)) {
					return error.errorReturn({
						message: "Sell in parameter not found, please input budget sell in or generate actual sell in",
					});
				}

				let existingClaim = 0;

				let condition = {};
				condition.is_active = true;
				condition.account_code = ttp.account_code;
				condition.type = "tt";

				const newExistingClaim = await Claim_F.aggregate([
					{
						$match: condition,
					},
					{
						$group: {
							_id: {
								account_code: "$account_code",
								periode_claim: "$periode_claim",
								point_trading_terms: "$point_trading_terms",
							},
							total: {
								$sum: "$amount",
							},
						},
					},
				]);

				if (newExistingClaim) {
					newExistingClaim.forEach((val, i) => {
						if (
							val._id.account_code == ttp.account_code &&
							val._id.periode_claim == payload.periode_claim + "-01" &&
							val._id.point_trading_terms.name == pointSelect.name &&
							val._id.point_trading_terms.value == pointSelect.value &&
							val._id.point_trading_terms.type == pointSelect.type
						) {
							existingClaim = val.total;
						}
					});
				}

				if (pointSelect.type == "percentage") {
					if (pointSelect.value != 0) {
						// Search harga actual dari const param
						const hasilPersen =
							Math.round(param.actual[month - 1] * (pointSelect.value / 100)) - existingClaim;

						if (payload.amount > hasilPersen) {
							return {
								data: `Max Claim ${hasilPersen.toLocaleString("id-ID")}`,
								status: "amount",
							};
						}

						debug(hasilPersen, " hasil persen");
					}
				} else if (pointSelect.type == "nominal") {
					const hasilNominal = pointSelect.value - existingClaim;

					if (payload.amount > hasilNominal) {
						return {
							data: `Max Claim ${hasilNominal.toLocaleString("id-ID")}`,
							status: "amount",
						};
					}

					debug(hasilNominal, " hasil nominal");
				}
			}

			create.point_trading_terms = ttp.point[payload.code];
			create.account_name = ttp.account_name;
			create.account_code = ttp.account_code;
		}

		const brand = await BrandModel.findOne({
			_id: ObjectId(payload.brand),
		}).lean();
		//check brand
		if (helper.isEmptyObject(brand)) {
			return error.errorReturn({
				message: "Brand not found",
			});
		}

		create.brand = brand;

		const claims = await new ClaimsModel(create).save();

		if (payload.type === "skp") {
			await generatedRealisationModule.generateActualSkpTt({
				type: "skp",
				date: payload.periode_claim + "-01",
			});
		} else {
			await generatedRealisationModule.generateActualSkpTt({
				type: "tt",
				date: payload.periode_claim + "-01",
			});
		}

		return { data: claims, status: "Success data has created" };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function readAlltypeSkp(query) {
	try {
		let condition = {
			is_active: true,
			type: "skp",
		};

		if (query?.store_code) {
			condition.account_code = query.store_code;
		}

		if (query?.datee) {
			condition.periode_claim = { $regex: ".*" + query.datee + ".*" };
		}

		// sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "created_at";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					created_at: -1,
			  };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;
		const claims = await ClaimsModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -created_at -updated_at -is_deleted")
			.lean();

		const claimsAll = await ClaimsModel.find(condition).lean();
		if (helper.isEmptyObject(claims)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		claims.forEach(async (_claims, index) => {
			let _master_point_result = {
				skp_number: _claims.skp.skp_number,
				skp: {
					value: _claims.skp.skp_number,
					text: _claims.skp.program_name,
					label: `${_claims.skp.skp_number} - ${_claims.skp.program_name}`,
				},
				brand: {
					value: _claims?.brand?._id,
					text: _claims?.brand?.name,
					label: _claims?.brand?.name,
				},
				account_name: _claims.account_name,
				amount: _claims.amount,
				distributor: _claims.distributor,
				invoice_number: _claims.invoice_number,
				periode_claim: _claims.periode_claim,
				promo_type: _claims.promo_type,
				remark: _claims.remark,
				_id: _claims._id,
			};
			if (Number(moment(_claims.created_at).format("YYYY")) === Number(moment().format("YYYY"))) {
				if (Number(moment(_claims.created_at).format("MM")) < Number(moment().format("MM"))) {
					_master_point_result.isDisable = true;
				} else {
					_master_point_result.isDisable = false;
				}
			} else {
				_claims_result.isDisable = true;
			}
			result.push(_master_point_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(claimsAll).length / sizePerPage),
			countData: Object.keys(claimsAll).length,
		};
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function readAlltypeTTP(query) {
	try {
		let condition = {
			is_active: true,
			type: "tt",
		};

		if (query?.store_code) {
			condition.account_code = query.store_code;
		}

		if (query?.datee) {
			condition.periode_claim = { $regex: ".*" + query.datee + ".*" };
		}
		// sort
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy ? query.sortBy : "created_at";
		const sort = query.sortBy
			? {
					[sortField]: sortOrder,
			  }
			: {
					created_at: -1,
			  };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;
		//limit
		const limit = sizePerPage;
		const claims = await ClaimsModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -created_at -updated_at -is_deleted")
			.lean();

		const claimsAll = await ClaimsModel.find(condition).lean();
		if (helper.isEmptyObject(claims)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		claims.forEach(async (_claims, index) => {
			let _master_point_result = {
				account: _claims.account_name,
				trading_term_points: `${_claims.point_trading_terms?.name} - ${_claims.point_trading_terms?.value} ${
					_claims.point_trading_terms?.type === "nominal" ? "IDR" : "%"
				}`,
				account_name: _claims.account_name,
				amount: _claims.amount,
				periode_claim: _claims.periode_claim,
				remark: _claims.remark,
				distributor: _claims.distributor,
				invoice_number: _claims.invoice_number,
				brand: {
					value: _claims?.brand?._id,
					text: _claims?.brand?.name,
					label: _claims?.brand?.name,
				},
				_id: _claims._id,
			};
			if (Number(moment(_claims.created_at).format("YYYY")) === Number(moment().format("YYYY"))) {
				if (Number(moment(_claims.created_at).format("MM")) < Number(moment().format("MM"))) {
					_master_point_result.isDisable = true;
				} else {
					_master_point_result.isDisable = false;
				}
			} else {
				_claims_result.isDisable = true;
			}
			result.push(_master_point_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(claimsAll).length / sizePerPage),
			countData: Object.keys(claimsAll).length,
		};
	} catch (error) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function deleteClaims(payload) {
	try {
		const dataClaim = await ClaimsModel.findOne({ _id: payload._id });
		const validate = validateIdSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };
		const update = {
			is_active: false,
		};
		let claimsDelete = await ClaimsModel.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(claimsDelete)) {
			return error.errorReturn({ message: "Claims not found" });
		}

		if (dataClaim.type === "skp") {
			await generatedRealisationModule.generateActualSkpTt({
				type: "skp",
				date: dataClaim.periode_claim,
			});
		} else {
			await generatedRealisationModule.generateActualSkpTt({
				type: "tt",
				date: dataClaim.periode_claim,
			});
		}

		return { data: claimsDelete, status: "Data has been deleted successfully" };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function updateClaims(payload) {
	try {
		const filterupdate = { _id: ObjectId(payload._id) };

		const create = {
			is_active: true,
			periode_claim: `${payload.periode_claim}-01`,
			amount: payload.amount,
			remark: payload.remark,
			type: payload.type,
			distributor: payload.distributor,
			invoice_number: payload.invoice_number,
			branch: payload.branch,
		};

		if (payload.type === "skp") {
			const filter = {
				skp_number: payload.code,
				is_active: true,
			};
			const skp = await SkpModel.findOne(filter).lean();
			create.skp = skp;
			create.account_name = skp.account_name;
			create.promo_type = payload.promo_type;
			create.account_code = skp.account_code;
		} else {
			const year = moment(payload.periode_claim + "-01").format("YYYY");
			const month = moment(payload.periode_claim + "-01").format("M");

			const dataClaim = await ClaimsModel.findOne({ _id: ObjectId(payload._id) });
			const pointSelect = dataClaim.point_trading_terms;

			if (Object.keys(pointSelect).length != 0) {
				// Search account_code dari Parameter
				const param = await Parameter.findOne({
					is_active: true,
					type: "sell_in",
					year: year,
					store_code: dataClaim.account_code,
				});

				let existingClaim = 0;

				let condition = {};
				condition.is_active = true;
				condition.account_code = dataClaim.account_code;
				condition.type = "tt";
				condition._id = {
					$ne: ObjectId(payload._id),
				};

				const newExistingClaim = await Claim_F.aggregate([
					{
						$match: condition,
					},
					{
						$group: {
							_id: {
								account_code: "$account_code",
								periode_claim: "$periode_claim",
								point_trading_terms: "$point_trading_terms",
							},
							total: {
								$sum: "$amount",
							},
						},
					},
				]);

				if (newExistingClaim) {
					newExistingClaim.forEach((val, i) => {
						if (
							val._id.account_code == dataClaim.account_code &&
							val._id.periode_claim == payload.periode_claim + "-01" &&
							val._id.point_trading_terms.name == pointSelect.name &&
							val._id.point_trading_terms.value == pointSelect.value &&
							val._id.point_trading_terms.type == pointSelect.type
						) {
							existingClaim = val.total;
						}
					});
				}

				if (pointSelect.type == "percentage") {
					if (pointSelect.value != 0) {
						// Search harga actual dari const param
						const hasilPersen =
							Math.round(param.actual[month - 1] * (pointSelect.value / 100)) - existingClaim;

						if (payload.amount > hasilPersen) {
							return {
								data: `Max Claim ${hasilPersen.toLocaleString("id-ID")}`,
								status: "amount",
							};
						}

						debug(hasilPersen, " hasil persen");
					}
				} else if (pointSelect.type == "nominal") {
					const hasilNominal = pointSelect.value - existingClaim;

					if (payload.amount > hasilNominal) {
						return {
							data: `Max Claim ${hasilNominal.toLocaleString("id-ID")}`,
							status: "amount",
						};
					}

					debug(hasilNominal, " hasil nominal");
				}
			}
		}

		const brand = await BrandModel.findOne({
			_id: ObjectId(payload.brand),
		}).lean();

		create.brand = brand;

		let claims = await ClaimsModel.findOneAndUpdate(filterupdate, create, {
			useFindAndModify: false,
			new: true,
		});

		if (helper.isEmptyObject(claims)) {
			return error.errorReturn({ message: "Claims not found" });
		}

		if (payload.type === "skp") {
			await generatedRealisationModule.generateActualSkpTt({
				type: "skp",
				date: payload.periode_claim + "-01",
			});
		} else {
			await generatedRealisationModule.generateActualSkpTt({
				type: "tt",
				date: payload.periode_claim + "-01",
			});
		}

		return {
			data: [],
			status: "Data has been updated successfully",
		};
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function downloadExcel(payload, res) {
	try {
		let listArr = [];
		let listArrBrand = [];
		var wb = new xl.Workbook();

		const listBrand = await BrandModel.find();

		if (payload.type == "SKP") {
			const listSKP = await SkpModel.find({
				program_start: {
					$gte: new Date(`01/01/${payload.year}`),
					$lte: new Date(`12/31/${payload.year}`),
				},
			});

			listSKP.forEach(el => {
				if (el.product.length > 0) {
					listArr.push(`${el.skp_number}`);
				}
			});

			listBrand.forEach(el => {
				listArrBrand.push(`${el.name}`);
			});

			// Sheet SKP
			var ws = wb.addWorksheet("SKP");

			const header = [
				"Nomor SKP",
				"Periode Claim (YYYY/MM/DD)",
				"Promo type",
				"Amount",
				"Distributor",
				"Brand",
				"Invoice number",
				"remark",
			];
			header.forEach((element, index) => {
				ws.cell(1, index + 1).string(element);
			});

			ws.cell(2, 2, 100, 2).style({ numberFormat: "yyyy/mm/dd" });

			// Sheet NomorSKP
			var ws2 = wb.addWorksheet("NomorSKP");
			var ws3 = wb.addWorksheet("Brand");

			listArr.forEach((element, index) => {
				ws2.cell(1 + index, 1).string(element);
			});

			listArrBrand.forEach((element, index) => {
				ws3.cell(1 + index, 1).string(element);
			});

			const akhirCell = listArr.length + 1;
			const akhirCellBrand = listArrBrand.length + 1;

			// Validation
			ws.addDataValidation({
				type: "list",
				allowBlank: 1,
				sqref: "A2:A100",
				formulas: [`=NomorSKP!$A$1:$A$${akhirCell}`],
			});

			ws.addDataValidation({
				type: "list",
				allowBlank: 1,
				sqref: "F2:F100",
				formulas: [`=Brand!$A$1:$A$${akhirCellBrand}`],
			});

			wb.write("Template Import SKP.xlsx", res);
		} else {
			const listTT = await TradingTermPointsModel.find();

			listTT.forEach(el => {
				if (el.point.length > 0) {
					el.point.forEach(v => {
						listArr.push(`${el.account_name} - ${v.name} - ${v.value}`);
					});
				}
			});

			listBrand.forEach(el => {
				listArrBrand.push(`${el.name}`);
			});

			// Sheet Trading Terms
			var ws = wb.addWorksheet("Trading Terms");

			const header = [
				"Trading Term Point",
				"Periode Claim (YYYY/MM/DD)",
				"Amount",
				"Distributor",
				"Brand",
				"Invoice number",
				"remark",
			];
			header.forEach((element, index) => {
				ws.cell(1, index + 1).string(element);
			});

			ws.cell(2, 2, 100, 2).style({ numberFormat: "yyyy/mm/dd" });

			// Sheet TT Point
			var ws2 = wb.addWorksheet("TT_Point");
			var ws3 = wb.addWorksheet("Brand");

			listArr.forEach((element, index) => {
				ws2.cell(1 + index, 1).string(element);
			});

			listArrBrand.forEach((element, index) => {
				ws3.cell(1 + index, 1).string(element);
			});

			const akhirCell = listArr.length + 1;
			const akhirCellBrand = listArrBrand.length + 1;

			// Validation
			ws.addDataValidation({
				type: "list",
				allowBlank: 1,
				sqref: "A2:A100",
				formulas: [`=TT_Point!$A$1:$A$${akhirCell}`],
			});

			ws.addDataValidation({
				type: "list",
				allowBlank: 1,
				sqref: "E2:E100",
				formulas: [`=Brand!$A$1:$A$${akhirCellBrand}`],
			});

			wb.write("Template Import TT.xlsx", res);
		}
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function importExcel(payload) {
	try {
		let arrStatus = [];
		if (payload.type == "SKP") {
			let create = [];
			let periode_claim = [];
			for (el of payload.payload) {
				const skp = await SkpModel.findOne({ skp_number: el[0] });
				const date_format = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY-MM") + "-01";

				const year = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY");

				const month = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("MM");

				let brand = "";
				if (el[5] != "") {
					brand = await BrandModel.findOne({ name: { $eq: el[5] } });
				}

				if (skp != null) {
					if (moment(skp.program_start).format("YYYY") === year) {
						create.push({
							is_active: true,
							periode_claim: year + "-" + month + "-01",
							amount: el[3],
							remark: el[7],
							type: "skp",
							created_at: new Date(),
							skp: skp,
							promo_type: el[2],
							account_name: skp.account_name,
							account_code: skp.account_code,
							distributor: el[4],
							invoice_number: el[6],
							brand: brand,
							source: "import",
						});
						periode_claim.push(year + "-" + month + "-01");
					}
				}
			}

			await ClaimsModel.insertMany(create);
			let period = [...new Set(periode_claim)];
			for (p of period) {
				await generatedRealisationModule.generateActualSkpTt({
					type: "skp",
					date: p,
				});
			}

			return { status: "success" };
		}

		if (payload.type == "TT") {
			for (el of payload.payload) {
				const ttp = el[0].split(" - ");
				const date_format = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY-MM") + "-01";

				const year = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY");
				const month = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("MM");
				const monthOneDigit = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("M");

				const TTPM = await TradingTermPointsModel.findOne({
					account_name: ttp[0],
					year: year,
					is_active: true,
				});

				let brand = "";
				if (el[4] != "") {
					brand = await BrandModel.findOne({ name: { $eq: el[4] } });
				}

				if (TTPM != null) {
					// TTPM.point.forEach(async (itm) => {
					for (itm of TTPM.point) {
						if (itm.name == ttp[1] && itm.value == ttp[2]) {
							const param = await Parameter.findOne({
								is_active: true,
								type: "sell_in",
								year: year,
								store_code: TTPM.account_code,
							});

							let existingClaim = 0;

							let condition = {};
							condition.is_active = true;
							condition.account_code = TTPM.account_code;
							condition.type = "tt";

							const newExistingClaim = await Claim_F.aggregate([
								{
									$match: condition,
								},
								{
									$group: {
										_id: {
											account_code: "$account_code",
											periode_claim: "$periode_claim",
											point_trading_terms: "$point_trading_terms",
										},
										total: {
											$sum: "$amount",
										},
									},
								},
							]);

							if (newExistingClaim) {
								newExistingClaim.forEach((val, i) => {
									if (
										val._id.account_code == TTPM.account_code &&
										val._id.periode_claim == year + "-" + month + "-01" &&
										val._id.point_trading_terms.name == itm.name &&
										val._id.point_trading_terms.value == itm.value &&
										val._id.point_trading_terms.type == itm.type
									) {
										existingClaim = val.total;
									}
								});
							}

							if (itm.type == "percentage") {
								if (itm.value != 0) {
									// Search harga actual dari const param
									const hasilPersen =
										Math.round(param.actual[monthOneDigit - 1] * (itm.value / 100)) - existingClaim;

									if (el[2] > hasilPersen) {
										arrStatus.push(`Max Claim ${hasilPersen.toLocaleString("id-ID")}`);
									} else {
										await ClaimsModel.create({
											is_active: true,
											periode_claim: year + "-" + month + "-01",
											amount: el[2],
											remark: el[6],
											type: "tt",
											created_at: new Date(),
											point_trading_terms: itm,
											account_name: TTPM.account_name,
											account_code: TTPM.account_code,
											distributor: el[3],
											invoice_number: el[5],
											brand: brand,
											source: "import",
										});

										await generatedRealisationModule.generateActualSkpTt({
											type: "tt",
											date: year + "-" + month + "-01",
										});

										arrStatus.push(`OK`);
									}

									debug(hasilPersen, " hasil persen");
								}
							} else if (itm.type == "nominal") {
								const hasilNominal = itm.value - existingClaim;

								if (el[2] > hasilNominal) {
									arrStatus.push(`Max Claim ${hasilNominal.toLocaleString("id-ID")}`);
								} else {
									await ClaimsModel.create({
										is_active: true,
										periode_claim: year + "-" + month + "-01",
										amount: el[2],
										remark: el[6],
										type: "tt",
										created_at: new Date(),
										point_trading_terms: itm,
										account_name: TTPM.account_name,
										account_code: TTPM.account_code,
										distributor: el[3],
										invoice_number: el[5],
										brand: brand,
										source: "import",
									});

									await generatedRealisationModule.generateActualSkpTt({
										type: "tt",
										date: year + "-" + month + "-01",
									});

									arrStatus.push(`OK`);
								}

								debug(hasilNominal, " hasil nominal");
							}
						}
					}
				}
			}
			return { status: "success" };
		}

		return { status: "error" };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function checkImportExcel(payload) {
	try {
		let arrStatus = [];
		if (payload.type == "TT") {
			await DummyClaimsModel.deleteMany({});
			await DummyParametersModel.deleteMany({});
			let dataClaimsOriginal = await ClaimsModel.find({}, { _id: 0 });
			let dataParametersOriginal = await Parameter.find({}, { _id: 0 });
			await DummyClaimsModel.insertMany(dataClaimsOriginal);
			await DummyParametersModel.insertMany(dataParametersOriginal);

			for (el of payload.payload) {
				const ttp = el[0].split(" - ");
				const date_format = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY-MM") + "-01";

				const year = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("YYYY");
				const month = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("MM");
				const monthOneDigit = moment(new Date(Math.round((el[1] - 25569) * 864e5))).format("M");

				const TTPM = await TradingTermPointsModel.findOne({
					account_name: ttp[0],
					year: year,
					is_active: true,
				});

				if (TTPM != null) {
					// TTPM.point.forEach(async (itm) => {
					for (itm of TTPM.point) {
						if (itm.name == ttp[1] && itm.value == ttp[2]) {
							const param = await DummyParametersModel.findOne({
								is_active: true,
								type: "sell_in",
								year: year,
								store_code: TTPM.account_code,
							});

							let existingClaim = 0;

							let condition = {};
							condition.is_active = true;
							condition.account_code = TTPM.account_code;
							condition.type = "tt";

							const newExistingClaim = await DummyClaimsModel.aggregate([
								{
									$match: condition,
								},
								{
									$group: {
										_id: {
											account_code: "$account_code",
											periode_claim: "$periode_claim",
											point_trading_terms: "$point_trading_terms",
										},
										total: {
											$sum: "$amount",
										},
									},
								},
							]);

							if (newExistingClaim) {
								newExistingClaim.forEach((val, i) => {
									if (
										val._id.account_code == TTPM.account_code &&
										val._id.periode_claim == year + "-" + month + "-01" &&
										val._id.point_trading_terms.name == itm.name &&
										val._id.point_trading_terms.value == itm.value &&
										val._id.point_trading_terms.type == itm.type
									) {
										existingClaim = val.total;
									}
								});
							}

							if (itm.type == "percentage") {
								if (itm.value != 0) {
									// Search harga actual dari const param
									const hasilPersen =
										Math.round(param.actual[monthOneDigit - 1] * (itm.value / 100)) - existingClaim;

									if (el[2] > hasilPersen) {
										arrStatus.push(`Max Claim ${hasilPersen.toLocaleString("id-ID")}`);
									} else {
										await DummyClaimsModel.create({
											is_active: true,
											periode_claim: year + "-" + month + "-01",
											amount: el[2],
											remark: el[6],
											type: "tt",
											created_at: new Date(),
											point_trading_terms: itm,
											account_name: TTPM.account_name,
											account_code: TTPM.account_code,
											distributor: el[3],
											invoice_number: el[5],
											branch: el[4],
											source: "import",
										});

										await dummyGenerateActualSkpTt({
											type: "tt",
											date: year + "-" + month + "-01",
										});

										arrStatus.push(`OK`);
									}

									debug(hasilPersen, " hasil persen");
								}
							} else if (itm.type == "nominal") {
								const hasilNominal = itm.value - existingClaim;

								if (el[2] > hasilNominal) {
									arrStatus.push(`Max Claim ${hasilNominal.toLocaleString("id-ID")}`);
								} else {
									await DummyClaimsModel.create({
										is_active: true,
										periode_claim: year + "-" + month + "-01",
										amount: el[2],
										remark: el[6],
										type: "tt",
										created_at: new Date(),
										point_trading_terms: itm,
										account_name: TTPM.account_name,
										account_code: TTPM.account_code,
										distributor: el[3],
										invoice_number: el[5],
										branch: el[4],
										source: "import",
									});

									await dummyGenerateActualSkpTt({
										type: "tt",
										date: year + "-" + month + "-01",
									});

									arrStatus.push(`OK`);
								}

								debug(hasilNominal, " hasil nominal");
							}
						}
					}
				} else {
					arrStatus.push("trading term point not found in the entered claim period");
				}
			}
			return { status: "success", data: arrStatus };
		}

		return { status: "error" };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function dummyGenerateActualSkpTt(payload) {
	try {
		//get all data parameter with condition
		const parameter = await DummyParametersModel.find({
			is_active: true,
			type: payload.type == "skp" ? "on_top_budget" : "trading_terms",
			year: moment(new Date(payload.date)).format("YYYY"),
		}).lean();

		let condition = {};
		condition.is_active = true;
		condition.type = payload.type;
		condition.periode_claim = moment(new Date(payload.date)).format("YYYY-MM-01");

		//aggaregation in here
		const result_claim_total = await DummyClaimsModel.aggregate([
			{
				$match: condition,
			},
			{
				$group: {
					_id: {
						account_code: "$account_code",
						account_name: "$account_name",
						type: "$type",
						periode_claim: "$periode_claim",
					},
					total: {
						$sum: "$amount",
					},
				},
			},
		]);

		//merge parameter with total claim
		let generate_actual = parameter.map((x, index) => {
			let actual_dummy = x.actual;
			if (helper.isEmptyArray(x.actual)) {
				actual_dummy = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			}
			let find_claim_total = result_claim_total.find(ct => ct._id.account_code === x.store_code);
			// debug(find_claim_total);
			if (typeof find_claim_total !== "undefined") {
				actual_dummy.splice(moment(payload.date).format("M") - 1, 1, find_claim_total.total);
				return { ...x, actual: actual_dummy };
			}
		});

		const results = generate_actual.filter(element => {
			return element;
		});

		//update result of merger
		for (elm of results) {
			await DummyParametersModel.findOneAndUpdate(
				{
					_id: mongoose.Types.ObjectId(elm._id),
				},
				elm,
				{ useFindAndModify: false, new: true }
			);
		}

		return results;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	create,
	readAlltypeSkp,
	readAlltypeTTP,
	deleteClaims,
	updateClaims,
	downloadExcel,
	importExcel,
	checkImportExcel,
};
