const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-skp-tt:master-nka-module");
const helper = require("../../common/helper");
const db = require("../../models");
const MasterNkaModel = db.masrer_nka;

const logSlack = require("../../helpers/slack");

async function list(query) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		const sort = { name: 1 };

		const masterNka = await MasterNkaModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(masterNka)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let result = [];

		masterNka.forEach(async (_clasificationss, index) => {
			let _clasificationss_result = {
				value: _clasificationss.code,
				text: _clasificationss.name,
				label: _clasificationss.name,
			};
			result.push(_clasificationss_result);
		});

		return {
			countData: Object.keys(masterNka).length,
			foundData: result,
		};
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

async function listName(query) {
	try {
		//query string
		let condition = {
			is_active: true,
		};

		const sort = { name: 1 };

		const masterNka = await MasterNkaModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(masterNka)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let result = [];

		masterNka.forEach(async (_clasificationss, index) => {
			let _clasificationss_result = _clasificationss.name;
			result.push(_clasificationss_result);
		});

		return {
			countData: Object.keys(masterNka).length,
			foundData: result,
		};
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, query);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	list,
	listName,
};
