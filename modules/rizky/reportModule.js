const Joi = require("joi");
const error = require("../../common/errorMessage");
const debug = require("debug")("backend-skp-tt:report-module-rizky");
const mongoose = require("mongoose");
const helper = require("../../common/helper");
const db = require("../../models");
const moment = require("moment");
const TradingTerms = db.trading_term_points;
const SKP = db.skp;
const Claim_F = db.claim_F;
const axios = require("axios");
const { x } = require("joi");
const logSlack = require("../../helpers/slack");
const tt_month = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
async function detailSummaryExpenseTT(payload) {
	try {
		//tt
		const tradingterms = await TradingTerms.findOne({
			is_active: true,
			account_code: payload.store_code,
			year: moment(new Date(payload.date)).format("YYYY"),
		}).lean();

		let condition = {};
		condition.is_active = true;
		condition.account_code = payload.store_code;
		condition.type = "tt";

		//aggaregation in here
		const result_claim_total = await Claim_F.aggregate([
			{
				$match: condition,
			},
			{
				$group: {
					_id: {
						account_code: "$account_code",
						periode_claim: "$periode_claim",
						point_trading_terms: "$point_trading_terms",
					},
					total: {
						$sum: "$amount",
					},
				},
			},
		]);

		let gent_tt = [];
		let tt = {};
		if (tradingterms) {
			tradingterms.point.map((e, i) => {
				debug(e.name, "==> name");
				let find_claim_total = result_claim_total.find(ct => ct._id.point_trading_terms.name === e.name);

				if (typeof find_claim_total !== "undefined") {
					let result_tt_month = tt_month.map((el, i) => {
						let find_claim_total = result_claim_total.find(
							ct =>
								moment(ct._id.periode_claim).format("MM") === el &&
								ct._id.point_trading_terms.name === e.name
						);

						debug(find_claim_total);
						debug(e, "==> month");
						if (typeof find_claim_total !== "undefined") {
							return find_claim_total.total;
						}
						return 0;
					});

					gent_tt.push({ ...e, expense: result_tt_month });
				}
			});
			tt = { ...tradingterms, point: gent_tt };
		} else {
			tt = { point: gent_tt };
		}

		// skp
		const skp = await SKP.find({
			is_active: true,
			account_code: payload.store_code,
		}).lean();

		let conditionskp = {};
		conditionskp.is_active = true;
		conditionskp.account_code = payload.store_code;
		conditionskp.type = "skp";

		//aggaregation in here
		const result_claim_total_skp = await Claim_F.aggregate([
			{
				$match: conditionskp,
			},
			{
				$group: {
					_id: {
						account_code: "$account_code",
						periode_claim: "$periode_claim",
						skp: "$skp.skp_number",
					},
					total: {
						$sum: "$amount",
					},
				},
			},
		]);

		const skp_label = result_claim_total_skp
			.map(item => item._id.skp)
			.filter((value, index, self) => self.indexOf(value) === index);

		let gent_skp = [];
		skp_label.map((e, i) => {
			debug(e, "==> name");
			let find_claim_total = result_claim_total_skp.find(ct => ct._id.skp === e);

			if (typeof find_claim_total !== "undefined") {
				let find_skp_month = tt_month.map((el, i) => {
					let find_claim_total = result_claim_total_skp.find(
						ct => moment(ct._id.periode_claim).format("MM") == el && ct._id.skp == e
					);
					debug(find_claim_total);
					debug(el, "==> month");
					if (typeof find_claim_total !== "undefined") {
						return find_claim_total.total;
					}
					return 0;
				});

				gent_skp.push({ skp: e, expense: find_skp_month });
			}
		});
		return { tt: tt, skp: gent_skp };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err.response.data, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	detailSummaryExpenseTT,
};
