const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-skp-tt:report-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const db = require("../models");
const moment = require("moment");
const Parameter = db.parameter;
const Master_NKA = db.masrer_nka;
const Claim_F = db.claim_F;
const axios = require("axios");
const array_with_zero_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const logSlack = require("../helpers/slack");

async function summaryPerAccount(payload) {
	try {
		//SELL IN
		const parameter_sell_in = await Parameter.findOne({
			is_active: true,
			type: "sell_in",
			store_code: payload.store_code,
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.lean();

		let actual_sell_in = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		if (!helper.isEmptyArray(parameter_sell_in?.actual)) {
			actual_sell_in = parameter_sell_in.actual;
		}
		let achievement = [];
		parameter_sell_in?.target?.map((x, index) => {
			if (x) {
				achievement.push(x === 0 ? 0 : (actual_sell_in[index] / x) * 100);
			} else {
				achievement.push(x === 0 ? 0 : (actual_sell_in[index] / (index + 1)) * 100);
			}
			debug(actual_sell_in[index] + " / " + x);
		});

		let sell_in = { ...parameter_sell_in, achievement };

		if (helper.isEmptyObject(parameter_sell_in)) {
			sell_in = {
				store_name: payload.store_code,
				store_code: payload.store_code,
				year: moment(new Date(payload.date)).format("YYYY"),
				type: "sell_in",
				target: [],
				actual: [],
				achievement: [],
			};
		}

		//OTB / SKP
		const parameter_on_top_budget = await Parameter.findOne({
			is_active: true,
			type: "on_top_budget",
			store_code: payload.store_code,
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.lean();

		let actual_dummy_otb = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		if (!helper.isEmptyArray(parameter_on_top_budget?.actual)) {
			actual_dummy_otb = parameter_on_top_budget.actual;
		}

		let cost_ratio_otb = [];
		parameter_on_top_budget?.actual?.map((x, index) => {
			cost_ratio_otb.push(
				actual_sell_in[index] === 0 ? 0 : parseFloat(((x / actual_sell_in[index]) * 100).toFixed(2))
			);
		});

		let otb = { ...parameter_on_top_budget, cost_ratio_otb };
		if (helper.isEmptyObject(parameter_on_top_budget)) {
			otb = {
				store_name: payload.store_code,
				store_code: payload.store_code,
				year: moment(new Date(payload.date)).format("YYYY"),
				type: "sell_in",
				target: [],
				actual: [],
				cost_ratio_otb: [],
			};
		}

		//TRADING TERMS
		const parameter_trading_terms = await Parameter.findOne({
			is_active: true,
			type: "trading_terms",
			store_code: payload.store_code,
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.lean();

		let cost_ratio_tt = [];
		parameter_trading_terms?.actual?.map((x, index) => {
			cost_ratio_tt.push(
				actual_sell_in[index] === 0 ? 0 : parseFloat(((x / actual_sell_in[index]) * 100).toFixed(2))
			);
		});

		let tt = { ...parameter_trading_terms, cost_ratio_tt };
		if (helper.isEmptyObject(parameter_trading_terms)) {
			tt = {
				store_name: payload.store_code,
				store_code: payload.store_code,
				year: moment(new Date(payload.date)).format("YYYY"),
				type: "sell_in",
				target: [],
				actual: [],
				cost_ratio_tt: [],
			};
		}

		debug(otb);
		return { sell_in, otb, tt };
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function summaryAll(payload) {
	try {
		//GET ALL STORE
		const all_store = await Master_NKA.find({ is_active: true }).sort({ name: 1 }).lean();

		//SELL IN
		const parameter_sell_in = await Parameter.find({
			is_active: true,
			type: "sell_in",
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.sort({ store_name: 1 })
			.lean();

		//SKP . OTB
		const parameter_skp_otb = await Parameter.find({
			is_active: true,
			type: "on_top_budget",
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.sort({ store_name: 1 })
			.lean();

		//TRADING TERMS
		const parameter_trading_terms = await Parameter.find({
			is_active: true,
			type: "trading_terms",
			year: moment(new Date(payload.date)).format("YYYY"),
		})
			.select("-store_group -store_segment -is_active -_id -year_iso")
			.sort({ store_name: 1 })
			.lean();

		//EXPENSE ALL
		let expense_all = [];
		all_store.map(x => {
			let find_claim_skp = parameter_skp_otb.find(ct => ct.store_code === x.code);
			let find_claim_tt = parameter_trading_terms.find(ctt => ctt.store_code === x.code);

			let data_klaim_skp = array_with_zero_data;
			let data_klaim_tt = array_with_zero_data;
			if (typeof find_claim_skp !== "undefined") {
				data_klaim_skp = find_claim_skp.actual;
			}
			if (typeof find_claim_tt !== "undefined") {
				data_klaim_tt = find_claim_tt.actual;
			}

			let result_sum = data_klaim_skp.map((y, y_index) => {
				return y + data_klaim_tt[y_index];
			});

			const sum = result_sum.reduce((accm, value) => {
				return accm + value;
			}, 0);

			if (sum > 0) {
				expense_all.push({
					store_name: x.name,
					store_code: x.code,
					year: moment(new Date(payload.date)).format("YYYY"),
					expense: result_sum,
				});
			}
		});

		//COST RATIO ALL
		let cost_ratio_all = expense_all.map(x => {
			let find_sell_in = parameter_sell_in.find(ct => ct.store_code === x.store_code);
			let actual_sell_in = array_with_zero_data;
			if (typeof find_sell_in !== "undefined") {
				actual_sell_in = find_sell_in.actual;
			}

			let hasil_cost_ratio = [];
			x.expense.map((y, y_index) => {
				hasil_cost_ratio.push(
					actual_sell_in[y_index] === 0 ? 0 : parseFloat(((y / actual_sell_in[y_index]) * 100).toFixed(2))
				);
			});
			return { ...x, sell_in: actual_sell_in, cost_ratio: hasil_cost_ratio };
		});

		//COST RATIO SKP
		let cost_ratio_skp = parameter_skp_otb.map(x => {
			let find_sell_in = parameter_sell_in.find(ct => ct.store_code === x.store_code);
			let actual_sell_in = array_with_zero_data;
			if (typeof find_sell_in !== "undefined") {
				actual_sell_in = find_sell_in.actual;
			}

			let hasil_cost_ratio = [];
			x.actual.map((y, y_index) => {
				hasil_cost_ratio.push(
					actual_sell_in[y_index] === 0 ? 0 : parseFloat(((y / actual_sell_in[y_index]) * 100).toFixed(2))
				);
			});
			return { ...x, sell_in: actual_sell_in, cost_ratio: hasil_cost_ratio };
		});

		//COST RATIO TT
		let cost_ratio_tt = parameter_trading_terms.map(x => {
			let find_sell_in = parameter_sell_in.find(ct => ct.store_code === x.store_code);
			let actual_sell_in = array_with_zero_data;
			if (typeof find_sell_in !== "undefined") {
				actual_sell_in = find_sell_in.actual;
			}

			let hasil_cost_ratio = [];
			x.actual.map((y, y_index) => {
				hasil_cost_ratio.push(
					actual_sell_in[y_index] === 0 ? 0 : parseFloat(((y / actual_sell_in[y_index]) * 100).toFixed(2))
				);
			});
			return { ...x, sell_in: actual_sell_in, cost_ratio: hasil_cost_ratio };
		});

		debug(cost_ratio_tt);

		return {
			sell_in: parameter_sell_in,
			expense_skp: cost_ratio_skp,
			expense_tt: cost_ratio_tt,
			expense_all: cost_ratio_all,
		};
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	summaryPerAccount,
	summaryAll,
};
