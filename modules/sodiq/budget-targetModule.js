const Joi = require("joi");
const error = require("../../common/errorMessage");
const db = require("../../models");
const BudgetTargetModel = db.budget_target;
const MasterNkaModel = db.masrer_nka;
const moment = require("moment");
const helper = require("../../common/helper");
const array_with_zero_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const debug = require("debug")("backend-skp-tt:budget-target-module");
const logSlack = require("../../helpers/slack");

const createSchema = Joi.object({
	store_name: Joi.string().required(),
	store_code: Joi.string().required(),
	store_group: Joi.string().valid("").required(),
	store_segment: Joi.string().valid("").required(),
	year: Joi.string().required(),
	year_iso: Joi.date().required(),
	is_active: Joi.bool().required(),
	type: Joi.string().required(),
	target: Joi.array().required(),
	actual: Joi.array().required(),
});

function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}

async function create(payload_params) {
	let payload = payload_params;
	let insert_data = [];
	let validate_status = false;
	let validate_message = "";

	try {
		//mapping payload to collection
		payload.data.forEach(async (_data_import, index) => {
			let _target_data = [];
			const arr_target = _data_import.slice(2, 14);
			array_with_zero_data.forEach((item, i) => {
				_target_data.push(arr_target[i]);
			});

			let _data_insert_result = {
				store_name: _data_import[0],
				store_code: _data_import[1],
				store_group: "",
				store_segment: "",
				year: payload.year,
				year_iso: new Date(),
				is_active: true,
				type: payload.type,
				target: _target_data,
				actual: array_with_zero_data,
			};
			insert_data.push(_data_insert_result);

			const validate = validateCreateSchema(_data_insert_result);
			if (validate.error) {
				validate_status = true;
				validate_message = validate.error.details[0].message;
			}
		});

		if (validate_status) {
			return error.errorReturn({ message: validate_message });
		}

		insert_data.forEach((item, i) => {
			let condition = {
				is_active: true,
				type: item.type,
				year: item.year,
				store_code: item.store_code,
			};
			BudgetTargetModel.updateMany(
				condition,
				{
					$set: {
						is_active: false,
					},
				},
				(err, docs) => {
					if (err) {
						return error.errorReturn({ message: err });
					} else {
						const result = new BudgetTargetModel(item).save();
					}
				}
			);
		});
		return { status: "success " };
	} catch (err) {
		debug("error create " + err);
		logSlack(arguments.callee.name, err, payload_params);
		return error.errorReturn({ message: err });
	}
}

async function readTable(query) {
	let condition = {
		is_active: true,
	};

	if (query.type !== "" && query.type !== undefined) {
		condition.type = query.type;
	}
	if (query.year !== "" && query.year !== undefined) {
		condition.year = query.year;
	}

	// sort
	const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
	const sortField = query.sortBy ? query.sortBy : "year_iso";
	const sort = query.sortBy
		? {
				[sortField]: sortOrder,
		  }
		: {
				year_iso: -1,
		  };

	// skip
	const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
	const page = query.page ? Number(query.page) : 1;
	const skip = sizePerPage * page - sizePerPage;

	//limit
	const limit = sizePerPage;
	try {
		const budgetTarget = await BudgetTargetModel.find(condition)
			.sort(sort)
			.skip(skip)
			.limit(limit)
			.select("-__v -created_at -updated_at -is_deleted")
			.lean();

		const allData = await BudgetTargetModel.find(condition).lean();
		if (helper.isEmptyObject(budgetTarget)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		let result = [];

		function total(target) {
			let ttl = 0;
			target.forEach(async tar => {
				ttl += tar;
			});
			return ttl;
		}

		budgetTarget.forEach(async (_bugetTarget, index) => {
			let _budegte_target_result = {
				_id: _bugetTarget._id,
				name: _bugetTarget.store_name,
				year: _bugetTarget.year,
				type: _bugetTarget.type,
				jan: _bugetTarget.target[0],
				feb: _bugetTarget.target[1],
				mar: _bugetTarget.target[2],
				apr: _bugetTarget.target[3],
				may: _bugetTarget.target[4],
				jun: _bugetTarget.target[5],
				jul: _bugetTarget.target[6],
				aug: _bugetTarget.target[7],
				sep: _bugetTarget.target[8],
				oct: _bugetTarget.target[9],
				nov: _bugetTarget.target[10],
				dec: _bugetTarget.target[11],
				total: total(_bugetTarget.target),
			};
			result.push(_budegte_target_result);
		});

		return {
			foundData: result,
			currentPage: page,
			countPages: Math.ceil(Object.keys(allData).length / sizePerPage),
			countData: Object.keys(allData).length,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err, query);
		debug("error read table : " + err);
	}
}

async function download(payload) {
	try {
		let condition = {
			is_active: true,
		};

		const sort = { name: 1 };
		const masterNka = await MasterNkaModel.find(condition).sort(sort).lean();

		let result = [];

		masterNka.forEach(async (_clasificationss, index) => {
			let _clasificationss_result = {
				code: _clasificationss.code,
				name: _clasificationss.name,
				jan: "",
				feb: "",
				mar: "",
				apr: "",
				may: "",
				jun: "",
				jul: "",
				aug: "",
				sep: "",
				oct: "",
				nov: "",
				dec: "",
			};
			result.push(_clasificationss_result);
		});

		return result;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload);
		debug("error download : " + err);
	}
}

module.exports = {
	create,
	readTable,
	download,
};
