const error = require("../../common/errorMessage");
const db = require("../../models");
const Parameter = db.parameter;
const helper = require("../../common/helper");
const logSlack = require("../../helpers/slack");

async function listYear() {
	try {
		let condition = {
			is_active: true,
		};
		const allData = await Parameter.find(condition).sort({ year: 1 }).lean();
		if (helper.isEmptyObject(allData)) {
			return {
				foundData: [],
			};
		}

		let result = [];
		let temp = "";

		allData.forEach(async _allData => {
			let year_result = {
				text: _allData.year,
				value: _allData.year,
				label: _allData.year,
			};
			if (temp !== _allData.year) {
				result.push(year_result);
			}
			temp = _allData.year;
		});

		return {
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	listYear,
};
