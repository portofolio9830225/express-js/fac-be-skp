const helper = require("../../common/helper");
const db = require("../../models");
const BrandModel = db.brand;
const error = require("../../common/errorMessage");
const logSlack = require("../../helpers/slack");

async function list() {
	try {
		let condition = {
			is_active: true,
		};

		const sort = { name: 1 };

		const brand = await BrandModel.find(condition).sort(sort).lean();
		if (helper.isEmptyObject(brand)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}
		let result = [];

		brand.forEach(async (_clasificationss, index) => {
			let _clasificationss_result = {
				value: _clasificationss._id,
				text: _clasificationss.name,
				label: _clasificationss.name,
			};
			result.push(_clasificationss_result);
		});

		return {
			countData: Object.keys(brand).length,
			foundData: result,
		};
	} catch (err) {
		logSlack(arguments.callee.name, err.response.data);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	list,
};
