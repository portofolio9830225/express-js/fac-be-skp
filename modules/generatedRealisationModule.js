const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-skp-tt:generate-realization-module");
const mongoose = require("mongoose");
const helper = require("../common/helper");
const db = require("../models");
const moment = require("moment");
const Parameter = db.parameter;
const Claim_F = db.claim_F;
const axios = require("axios");

const logSlack = require("../helpers/slack");

async function generateActualSellIn(payload) {
	try {
		const parameter = await Parameter.find({
			is_active: true,
			type: "sell_in",
			year: moment(new Date(payload.date)).format("YYYY"),
		}).lean();

		//get all nka sell_in
		const dataSellIn = await getApiNkaSellIn({
			year: moment(new Date(payload.date)).format("YYYY"),
			app_token: payload.app_token,
			user_token: payload.user_token,
		});

		let generate_actual = parameter.map((x, index) => {
			let actual_dummy = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			var sell_in_per_acc = dataSellIn.filter(function (el) {
				return el.group_nka == x.store_code;
			});
			debug(sell_in_per_acc, index);
			if (!helper.isEmptyArray(sell_in_per_acc)) {
				sell_in_per_acc.map(y => {
					actual_dummy.splice(moment(y.date).format("M") - 1, 1, y.sell_in);
				});
				return { ...x, actual: actual_dummy };
			}
		});

		for (elm of generate_actual) {
			await Parameter.findOneAndUpdate(
				{
					_id: mongoose.Types.ObjectId(elm._id),
				},
				elm,
				{ useFindAndModify: false, new: true }
			);
		}

		return generate_actual;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function generateActualSkpTt(payload) {
	try {
		//get all data parameter with condition
		const parameter = await Parameter.find({
			is_active: true,
			type: payload.type == "skp" ? "on_top_budget" : "trading_terms",
			year: moment(new Date(payload.date)).format("YYYY"),
		}).lean();

		let condition = {};
		condition.is_active = true;
		condition.type = payload.type;
		condition.periode_claim = moment(new Date(payload.date)).format("YYYY-MM-01");

		//aggaregation in here
		const result_claim_total = await Claim_F.aggregate([
			{
				$match: condition,
			},
			{
				$group: {
					_id: {
						account_code: "$account_code",
						account_name: "$account_name",
						type: "$type",
						periode_claim: "$periode_claim",
					},
					total: {
						$sum: "$amount",
					},
				},
			},
		]);

		//merge parameter with total claim
		let generate_actual = parameter.map((x, index) => {
			let actual_dummy = x.actual;
			if (helper.isEmptyArray(x.actual)) {
				actual_dummy = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			}
			let find_claim_total = result_claim_total.find(ct => ct._id.account_code === x.store_code);
			debug(find_claim_total);
			if (typeof find_claim_total !== "undefined") {
				actual_dummy.splice(moment(payload.date).format("M") - 1, 1, find_claim_total.total);
				return { ...x, actual: actual_dummy };
			}
			actual_dummy.splice(moment(payload.date).format("M") - 1, 1, 0);
			return { ...x, actual: actual_dummy };
		});

		const results = generate_actual.filter(element => {
			return element;
		});

		//update result of merger
		for (elm of results) {
			await Parameter.findOneAndUpdate(
				{
					_id: mongoose.Types.ObjectId(elm._id),
				},
				elm,
				{ useFindAndModify: false, new: true }
			);
		}

		return results;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function getApiNkaSellIn(query) {
	try {
		let url = `${process.env.DISTRIBUTOR_CONNECTION_URL}distributor-connection/sell_in/nka/?year=${query.year}`;
		let parameters = {
			method: "GET",
			url: url,
			headers: {
				"x-public-key": `${process.env.PUBLIC_KEY}`,
				"x-app-name": `${process.env.NAME}`,
				"x-application-token": `${query.app_token}`,
				"X-User-Token": `${query.user_token}`,
				"Content-Type": "application/json",
			},
		};

		const result = await axios(parameters)
			.then(function async(response) {
				return response.data;
			})
			.catch(function (error) {
				debug(error, "=> error callApiByArea");
				return [];
			});
		return result;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, query);
		return [];
	}
}

module.exports = {
	generateActualSellIn,
	generateActualSkpTt,
};
