const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-skp-tt:menu-module");
const helper = require("../common/helper");
const db = require("../models");
const Organizations = db.organizations;
const logSlack = require("../helpers/slack");
const { ObjectId } = require("mongodb");
const createSchema = Joi.object({
	organization_name: Joi.string().required(),
	requester: Joi.string().required(),
});

const updateSchema = Joi.object({
	_id: Joi.string().required(),
	organization_name: Joi.string().required(),
	requester: Joi.string().required(),
});

const idSchema = Joi.object({
	_id: Joi.string().required(),
});
function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}
function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

const ID = "ORG";

async function create(payload) {
	try {
		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const filter = {
			is_active: true,
			organization_name: payload.organization_name,
		};

		const existing = await Organizations.findOne(filter).lean();

		if (!helper.isEmptyObject(existing)) {
			return error.errorReturn({ message: "Organization already Exists!" });
		}

		const new_orgs = { ...payload };

		new_orgs.organization_code = await generateCode();
		new_orgs.created_at = new Date();
		new_orgs.created_by = payload.requester;

		const result = await new Organizations(new_orgs).save();

		return result;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function read(query) {
	try {
		// filter
		const filter = { is_active: true };

		// sort order
		const sortOrder = query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
		const sortField = query.sortBy;
		const sort = { [sortField]: sortOrder };

		// skip
		const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 10;
		const page = query.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		// limit
		const limit = sizePerPage;

		// find by filter
		const found_data = await Organizations.find(filter)
			.sort(sort)
			.collation({ locale: "en_US", numericOrdering: true })
			.skip(skip)
			.limit(limit)
			.select("-__v");

		// check array object value
		if (helper.isEmptyArray(found_data)) {
			return {
				foundData: [],
				currentPage: page,
				countPages: 0,
				countData: 0,
			};
		}

		// count data
		const count_data = await Organizations.countDocuments(filter);
		const result = {
			foundData: found_data,
			currentPage: page,
			countPages: Math.ceil(count_data / sizePerPage),
			countData: count_data,
		};

		return result;
	} catch (e) {
		debug(e, "===> ERROR READ LOCATION");
		logSlack(arguments.callee.name, e, query);
		throw error.errorReturn({ message: "Internal server error" });
	}
}
async function update(payload) {
	try {
		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const filter = {
			is_active: true,
			organization_code: payload.organization_code,
		};

		const existing = await Organizations.findOne(filter).lean();

		if (helper.isEmptyObject(existing)) {
			return error.errorReturn({ message: "Organization already Exists!" });
		}

		const new_orgs = { ...payload };
		new_orgs.updated_at = new Date();
		new_orgs.updated_by = payload.requester;
		new_orgs.is_active = true;

		const result = await Organizations.findOneAndUpdate(filter, new_orgs, {
			useFindAndModify: true,
			new: false,
		});

		return result;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function destroy(payload) {
	try {
		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}

		const filter = {
			is_active: true,
			organization_code: payload.organization_code,
		};

		const existing = await Organizations.findOne(filter).lean();

		if (helper.isEmptyObject(existing)) {
			return error.errorReturn({ message: "Organization already Exists!" });
		}

		const new_orgs = { ...payload };
		new_orgs.deleted_at = new Date();
		new_orgs.deleted_by = payload.requester;
		new_orgs.is_active = false;

		const result = await Organizations.findOneAndUpdate(filter, new_orgs, {
			useFindAndModify: true,
			new: false,
		});

		return result;
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

async function generateCode() {
	try {
		const all_orgs = await Organizations.find({}).sort({
			organization_code: -1,
		});
		const last_id = parseInt(all_orgs[0].organization_code.replace(ID, ""));
		const new_id = last_id++;
		return ID + new_id.toString();
	} catch (err) {
		debug(err);
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	create,
	read,
	update,
	destroy,
};
