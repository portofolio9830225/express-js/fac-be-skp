const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-skp-tt:submenu-module");
const helper = require("../common/helper");
const logSlack = require("../helpers/slack");
const db = require("../models");
const Submenu = db.submenu;
const Menu = db.menu;

const createSchema = Joi.object({
	name: Joi.string().required(),
	link: Joi.string().required(),
	isDeleted: Joi.boolean(),
});

const updateSchema = Joi.object({
	_id: Joi.string().required(),
	name: Joi.string(),
	link: Joi.string(),
});

const idSchema = Joi.object({
	_id: Joi.string().required(),
});
function validateCreateSchema(schema) {
	return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
	return updateSchema.validate(schema);
}
function validateIdSchema(schema) {
	return idSchema.validate(schema);
}

async function createSubmenu(payload_params) {
	try {
		let payload = payload_params;
		const menuId = payload._id;
		delete payload._id;

		const validate = validateCreateSchema(payload);
		if (validate.error) {
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		payload.isDeleted = false;
		payload.link = `/${payload.link}`;
		payload.created_at = new Date(`UTC+7`);
		payload.updated_at = new Date(`UTC+7`);
		payload.menu = menuId;
		const submenu = await new Submenu(payload).save();
		return submenu;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload_params);
		return error.errorReturn({ message: err.response.data });
	}
}

function updateSubmenu(payload) {
	try {
		const validate = validateUpdateSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };
		let update = payload;
		if (helper.hasKey(payload, "link")) {
			update.link = `/${payload.link}`;
		}
		let submenu = Submenu.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(submenu)) {
			return error.errorReturn({ message: "Submenu not found" });
		}

		return submenu;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}
function deleteSubmenu(payload) {
	try {
		const validate = validateIdSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };

		const update = {
			updated_at: new Date(`UTC+7`),
			isDeleted: true,
		};
		let submenu = Submenu.findOneAndUpdate(filter, update, {
			useFindAndModify: false,
			new: true,
		});
		if (helper.isEmptyObject(submenu)) {
			return error.errorReturn({ message: "Submenu not found" });
		}

		return submenu;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}
async function readAllSubmenu(payload) {
	try {
		payload.isDeleted = false;
		const filter = payload;
		let submenu = await Submenu.find(filter).cursor();
		if (helper.isEmptyObject(submenu)) {
			return error.errorReturn({ message: "Submenu not found" });
		}
		let result = [];

		for (let _submenu = await submenu.next(); _submenu != null; _submenu = await submenu.next()) {
			let _submenu_result = {
				name: _submenu.name,
				link: _submenu.link.replace("/", ""),
				_id: _submenu._id,
			};
			result.push(_submenu_result);
		}

		return result;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}
async function readByIdSubmenu(payload) {
	try {
		const validate = validateIdSchema(payload);
		if (validate.error) {
			debug(validate.error);
			return error.errorReturn({ message: validate.error.details[0].message });
		}
		const filter = { _id: payload._id };

		let submenu = await Submenu.findOne(filter);

		if (helper.isEmptyObject(submenu)) {
			return error.errorReturn({ message: "Submenu not found" });
		}

		return submenu;
	} catch (err) {
		logSlack(arguments.callee.name, err, payload);
		return error.errorReturn({ message: err.response.data });
	}
}

module.exports = {
	createSubmenu,
	updateSubmenu,
	deleteSubmenu,
	readByIdSubmenu,
	readAllSubmenu,
};
