const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:master-nka-route");
const BackendValidator = require("../../middlewares/BackendValidator");
const masterNkaModule = require("../../modules/rizky/master-nkaModule");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await masterNkaModule.list(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/list", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await masterNkaModule.listName(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
