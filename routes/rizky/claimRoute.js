const express = require("express");
const router = express.Router();
const helper = require("../../common/helper");
const BackendValidator = require("../../middlewares/BackendValidator");
const claimsModule = require("../../modules/rizky/claimsModule");
const debug = require("debug")("backend-skp-tt:claimRoute");

router.get("/skp", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		const result = await claimsModule.readAlltypeSkp(payload);
		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.get("/download-excel", BackendValidator.isValidRequest, async (req, res) => {
	const payload = req.query;
	await claimsModule.downloadExcel(payload, res);
});

router.get("/ttp", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		const result = await claimsModule.readAlltypeTTP(payload);
		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;

		const result = await claimsModule.create(payload);
		debug(result);
		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.post("/import-excel", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await claimsModule.importExcel(payload.data);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/check-import-excel", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await claimsModule.checkImportExcel(payload.data);

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await claimsModule.deleteClaims(payload);

		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await claimsModule.updateClaims(payload);
		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/check-skp-number", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		debug(payload);
		let result = {};
		result = await claimsModule.checkSkpNumber(payload);
		if (!result.error) {
			return res.send(result);
		} else {
			return res.status(400).send(result);
		}
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

module.exports = router;
