const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:*");
const helper = require("../../common/helper");
const BackendValidator = require("../../middlewares/BackendValidator");
const skpModule = require("../../modules/rizky/skpModule");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    const result = await skpModule.readTable(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      debug(result);
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/list", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const result = await skpModule.list();
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await skpModule.create(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await skpModule.deleteSkp(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await skpModule.updateSkp(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post(
  "/check-skp-number",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;
      debug(payload);
      let result = {};
      result = await skpModule.checkSkpNumber(payload);
      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

module.exports = router;
