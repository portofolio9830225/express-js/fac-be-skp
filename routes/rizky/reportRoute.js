const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:menu-routes");
const BackendValidator = require("../../middlewares/BackendValidator");
const reportModule = require("../../modules/rizky/reportModule");

router.get("/tt", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const x_app_token = req.header("x-application-token");
    const x_user_token = req.header("x-user-token");
    let payload = req.query;
    payload.app_token = x_app_token;
    payload.user_token = x_user_token;

    let result = {};
    result = await reportModule.detailSummaryExpenseTT(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
