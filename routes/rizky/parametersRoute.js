const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:parameter-route");
const BackendValidator = require("../../middlewares/BackendValidator");
const parameterModule = require("../../modules/rizky/parametersModule");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    let result = {};
    result = await parameterModule.listYear(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
