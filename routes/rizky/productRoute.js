const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:master-nka-route");
const BackendValidator = require("../../middlewares/BackendValidator");
const productModule = require("../../modules/rizky/productModule");

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await productModule.list(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
