const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:*");
const helper = require("../../common/helper");
const BackendValidator = require("../../middlewares/BackendValidator");
const TradingTermPointsModule = require("../../modules/rizky/trading-term-pointsModule");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await TradingTermPointsModule.create(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    const result = await TradingTermPointsModule.readTable(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/list", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await TradingTermPointsModule.listSelect(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/listyear", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await TradingTermPointsModule.listYear(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.post(
  "/check-store",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;
      const result = await TradingTermPointsModule.checkStore(payload);
      if (!result.error) {
        return res.send(result);
      } else {
        return res.status(400).send(result);
      }
    } catch (err) {
      return res.status(500).send(err);
    }
  }
);

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);
    const result = await TradingTermPointsModule.update(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await TradingTermPointsModule.deleteTTPoin(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
