const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-skp-tt:*");
const SummaryAccount = require("../../modules/sodiq/summary-accountModule");
const BackendValidator = require("../../middlewares/BackendValidator");

router.get("/year", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const result = await SummaryAccount.listYear();
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

module.exports = router;
