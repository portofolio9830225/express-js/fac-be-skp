const express = require("express");
const router = express.Router();
const BudgetTargetModule = require("../../modules/sodiq/budget-targetModule");
const BackendValidator = require("../../middlewares/BackendValidator");
const excel = require("exceljs");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    const result = await BudgetTargetModule.create(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    const result = await BudgetTargetModule.readTable(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

router.get("/download", async (req, res) => {
  try {
    const payload = req.query;
    const result = await BudgetTargetModule.download(payload);
    if (!result.error) {
      let workbook = new excel.Workbook();
      let worksheet = workbook.addWorksheet("Template Budget & Target");

      worksheet.columns = [
        { header: "Account Name", key: "name", width: 20 },
        { header: "Account Code", key: "code", width: 20 },
        { header: "Jan", key: "jan", width: 10 },
        { header: "Feb", key: "feb", width: 10 },
        { header: "Mar", key: "mar", width: 10 },
        { header: "Apr", key: "apr", width: 10 },
        { header: "May", key: "may", width: 10 },
        { header: "Jun", key: "jun", width: 10 },
        { header: "Jul", key: "jul", width: 10 },
        { header: "Aug", key: "aug", width: 10 },
        { header: "Sep", key: "sep", width: 10 },
        { header: "Oct", key: "oct", width: 10 },
        { header: "Nov", key: "nov", width: 10 },
        { header: "Dec", key: "dec", width: 10 },
      ];

      worksheet.addRows(result);
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "tutorials.xlsx"
      );
      return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
      });
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    return res.status(500).send(err);
  }
});

module.exports = router;
