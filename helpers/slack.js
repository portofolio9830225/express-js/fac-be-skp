const axios = require("axios");

const slack = async (req, err, data = "") => {
	return await axios.post("https://hooks.slack.com/services/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", {
		text: `*function :*\n${req}\n\n *data :*\n${JSON.stringify(data)}\n\n *error :*\n${err}`,
	});
};

module.exports = slack;
