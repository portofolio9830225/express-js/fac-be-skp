const env = require("dotenv").config();
process.env.NODE_ENV = "test";

const {
  bodyRoles,
  bodyRolesUpdate,
  schemaRoles,
} = require("./schemaValidator/roles_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:roles-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.roles.deleteMany();
  await db.roles_history.deleteMany();
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE, ROLE", () => {
  it("POST /api/roles", async () => {
    const { segment_code, requester } = bodyRoles;

    // create
    const createRole = await reqWithAuth("/api/roles", "post", bodyRoles);

    expect(createRole.status).toEqual(200);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // read
    const readRole = await reqWithAuth(
      `/api/roles?segment_code=${segment_code}`,
      "get"
    );
    expect(readRole.status).toEqual(200);
    expect(readRole.body).toMatchSchema(schemaRoles);

    // update
    const updateRole = await reqWithAuth("/api/roles", "put", {
      ...bodyRolesUpdate,
      _id: readRole.body.foundData[0]._id,
      role_code: readRole.body.foundData[0].role_code,
    });
    expect(updateRole.status).toEqual(200);
    expect(updateRole.body).toMatchSchema(schemaRoles);

    // delete
    const deleteRole = await reqWithAuth("/api/roles", "delete", {
      _id: readRole.body.foundData[0]._id,
      requester,
    });
    expect(deleteRole.status).toEqual(200);
    expect(deleteRole.body).toMatchSchema(schemaRoles);
  });
});

describe("CREATE, READ, UPDATE, DELETE, ROLE UNEXPECTED", () => {
  it("POST /api/roles", async () => {
    const segment_code = "";
    const requester = "";

    // create
    const createRole = await reqWithAuth("/api/roles", "post", {});
    expect(createRole.status).toEqual(400);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // read
    const readRole = await reqWithAuth(
      `/api/roles?segment_code=${segment_code}`,
      "get"
    );
    expect(readRole.status).toEqual(400);
    expect(readRole.body).toMatchSchema(schemaRoles);

    // update
    const updateRole = await reqWithAuth("/api/roles", "put", {
      ...bodyRolesUpdate,
      _id: "",
    });
    expect(updateRole.status).toEqual(400);
    expect(updateRole.body).toMatchSchema(schemaRoles);

    // delete
    const deleteRole = await reqWithAuth("/api/roles", "delete", {
      _id: "",
      requester,
    });
    expect(deleteRole.status).toEqual(400);
    expect(deleteRole.body).toMatchSchema(schemaRoles);
  });
});
