const env = require("dotenv").config();
process.env.NODE_ENV = "test";

const {
  bodyRoles,
  bodyRolesUpdate,
  schemaRoles,
} = require("./schemaValidator/roles_schemaValidator");

const {
  bodyApprovalSetting,
  bodyApprovalSettingUpdate,
  schemaApprovalSetting,
} = require("./schemaValidator/approvalSetting_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:approval-setting-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.roles.deleteMany();
  await db.roles_history.deleteMany();
  await db.approval_setting.deleteMany();
  await db.approval_setting_history.deleteMany();
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE, APPROVAL SETTING", () => {
  it("POST /api/approval-setting", async () => {
    const { segment_code, requester } = bodyRoles;

    // ========== ROLE START
    // create
    const createRole = await reqWithAuth("/api/roles", "post", bodyRoles);
    expect(createRole.status).toEqual(200);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // read
    const readRole = await reqWithAuth(
      `/api/roles?segment_code=${segment_code}`,
      "get"
    );
    expect(readRole.status).toEqual(200);
    expect(readRole.body).toMatchSchema(schemaRoles);
    // ========== ROLE END

    // ========== APPROVAL SETTING START
    // create
    const createApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "post",
      bodyApprovalSetting
    );
    debug(createApprovalSetting.body, "===> CREATE APPROVAL");
    expect(createApprovalSetting.status).toEqual(200);
    expect(createApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // read
    const readApprovalSetting = await reqWithAuth(
      `/api/approval-setting?segment_code=${segment_code}`,
      "get"
    );
    debug(readApprovalSetting.body, "===> READ APPROVAL");
    expect(readApprovalSetting.status).toEqual(200);
    expect(readApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // update
    const updateApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "put",
      {
        ...bodyApprovalSettingUpdate,
        _id: readApprovalSetting.body.foundData[0]._id,
      }
    );
    expect(updateApprovalSetting.status).toEqual(200);
    expect(updateApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // delete
    const deleteApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "delete",
      {
        _id: readApprovalSetting.body.foundData[0]._id,
        requester,
      }
    );
    expect(deleteApprovalSetting.status).toEqual(200);
    expect(deleteApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // ========== APPROVAL SETTING END
  });
});

describe("CREATE, READ, UPDATE, DELETE, APPROVAL SETTING UNEXPECTED", () => {
  it("POST /api/approval-setting", async () => {
    const { segment_code, requester } = bodyRoles;

    // ========== ROLE START
    // create
    const createRole = await reqWithAuth("/api/roles", "post", bodyRoles);
    expect(createRole.status).toEqual(200);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // read
    const readRole = await reqWithAuth(
      `/api/roles?segment_code=${segment_code}`,
      "get"
    );
    expect(readRole.status).toEqual(200);
    expect(readRole.body).toMatchSchema(schemaRoles);
    // ========== ROLE END

    // ========== APPROVAL SETTING START
    // create
    const createApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "post",
      bodyApprovalSetting
    );
    expect(createApprovalSetting.status).toEqual(200);
    expect(createApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // create exist
    const createApprovalSettingExt = await reqWithAuth(
      "/api/approval-setting",
      "post",
      bodyApprovalSetting
    );
    expect(createApprovalSettingExt.status).toEqual(400);
    expect(createApprovalSettingExt.body).toMatchSchema(schemaApprovalSetting);

    // read
    const readApprovalSetting = await reqWithAuth(
      `/api/approval-setting?segment=${segment_code}`,
      "get"
    );
    debug(readApprovalSetting.body, "===> READ APPROVAL");
    expect(readApprovalSetting.status).toEqual(400);
    expect(readApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // update
    const updateApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "put",
      {
        ...bodyApprovalSettingUpdate,
        _id: "",
      }
    );
    expect(updateApprovalSetting.status).toEqual(400);
    expect(updateApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // delete
    const deleteApprovalSetting = await reqWithAuth(
      "/api/approval-setting",
      "delete",
      {
        _id: "",
        requester,
      }
    );
    expect(deleteApprovalSetting.status).toEqual(400);
    expect(deleteApprovalSetting.body).toMatchSchema(schemaApprovalSetting);

    // ========== APPROVAL SETTING END
  });
});
