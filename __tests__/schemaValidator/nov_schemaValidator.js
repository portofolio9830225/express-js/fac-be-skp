require("dotenv").config();
const schemaStore = {
  type: "object",
  properties: {
    _id: { type: "string" },
    store_code: { type: "string" },
    store_name: { type: "string" },
    city_code: { type: "string" },
    city_name: { type: "string" },
    store_address: { type: "string" },
    google_address: { type: "string" },
    latitude: { type: "string" },
    longitude: { type: "string" },
    segment_code: { type: "string" },
    segment_name: { type: "string" },
    store_segment_code: { type: "string" },
    store_segment_name: { type: "string" },
    type: { type: "string" },
    category: { type: "string" },
    store_image_id: { type: "string" },
    postal_code: { type: "string" },
    product: { type: "array" },
    detail_segment: { type: "array" },
    pic: { type: "array" },
    remark: { type: "string" },
    distributor_gerai_id: { type: "string" },
    validated_by: { type: "string" },
    is_valid: { type: "boolean" },
    is_active: { type: "boolean" },
    created_at: { type: "string" },
    created_by: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
    id_history: { type: "string" },
  },
};

const bodyStore = {
  _parts: [
    ["requester", "LNK000632"],
    ["full_name", "Faizuddin Wasis Prabowo"],
    ["store_name", "TEST INPUT"],
    ["area_code", "0001-001-014"],
    ["city_code", "164"],
    ["role_code", "0002"],
    ["role_name", "ASS"],
    ["store_address", "TEST ALAMAT"],
    ["latitude", -7.343687],
    ["longitude", 112.4422802],
    ["segment_code", "0001"],
    ["segment_name", "Retail"],
    ["store_segment_code", "4"],
    ["type", "ST001"],
    ["category", "store"],
    ["remark", "Ini Contoh"],
  ],
};

const bodyStoreUpdate = {};

module.exports = {
  schemaStore,
  bodyStore,
  bodyStoreUpdate,
};
