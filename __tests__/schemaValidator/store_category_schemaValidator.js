const schemaStoreCategory = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    store_category_code: {
      type: "string",
    },
    store_category_name: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateStoreCateogory = {
  requester: "LNK000632",
  segment_code: "0001",
  segment: "Retail",
  store_category_code: "nasional",
  store_category_name: "Nasional",
};

const bodyUpdateStoreCateogory = {
  requester: "LNK000632",
  segment_code: "0001",
  segment: "Retail",
  store_category_code: "nasional",
  store_category_name: "Nasional",
};

module.exports = {
  schemaStoreCategory,
  bodyCreateStoreCateogory,
  bodyUpdateStoreCateogory,
};
