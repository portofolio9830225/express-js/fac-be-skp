require("dotenv").config();
const schemaApprovalSetting = {
  type: "object",
  properties: {
    _id: { type: "string" },
    is_active: { type: "boolean" },
    role_code: { type: "string" },
    role_name: { type: "string" },
    segment_code: { type: "string" },
    segment_name: { type: "string" },
    approval_category: { type: "string" },
    min_approval: { type: "string" },
    user: { type: "array" },
    created_at: { type: "string" },
    created_by: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
  },
};

const bodyApprovalSetting = {
  requester: "LNK000632",
  approval_category: "Upper Parent",
  min_approval: 1,
  segment_code: "0001",
  segment_name: "Retail",
  role_code: "0002",
  role_name: "ASS",
};

const bodyApprovalSettingUpdate = {
  requester: "LNK000632",
  approval_category: "Manual Setting",
  min_approval: 2,
  user: [
    { username: "LNK000632", email: "mis.stf05@lnk.co.id", department: "mis" },
    { username: "LNK000745", email: "mis.spv02@lnk.co.id", department: "mis" },
  ],
};

module.exports = {
  schemaApprovalSetting,
  bodyApprovalSetting,
  bodyApprovalSettingUpdate,
};
