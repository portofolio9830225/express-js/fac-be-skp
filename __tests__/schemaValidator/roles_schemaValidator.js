require("dotenv").config();
const schemaRoles = {
  type: "object",
  properties: {
    _id: { type: "string" },
    is_active: { type: "boolean" },
    username: { type: "string" },
    name: { type: "string" },
    email: { type: "string" },
    detail_user: { type: "object" },
    type: { type: "string" },
    department: { type: "string" },
    role: { type: "array" },
    created_at: { type: "string" },
    created_by: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
  },
};

const bodyRoles = {
  requester: "LNK000632",
  role_name: "Sales",
  segment_code: "0001",
  segment_name: "Horeca",
  store_segment: [],
};

const bodyRolesUpdate = {
  requester: "LNK000632",
  role_name: "Sales",
  segment_code: "0002",
  segment_name: "B2C",
  store_segment: [],
};

module.exports = {
  schemaRoles,
  bodyRoles,
  bodyRolesUpdate,
};
