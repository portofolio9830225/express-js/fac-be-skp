require("dotenv").config();
const schemaTarget = {
  type: "object",
  properties: {
    _id: { type: "string" },
    is_active: { type: "boolean" },
    segment_code: { type: "string" },
    segment_name: { type: "string" },
    target_name: { type: "string" },
    target_code: { type: "string" },
    type: { type: "string" },
    menu_name: { type: "string" },
    menu_code: { type: "string" },
    access_target: { type: "array" },
    created_at: { type: "string" },
    created_by: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
  },
};

const bodyCreate = {
  requester: "LNK000632",
  access_target: [
    { role_code: "0008", role_name: "SPG", default_target: 500 },
    { role_code: "0007", role_name: "SPG SELL OUT", default_target: 50 },
  ],
  segment_code: "0001",
  segment_name: "Retail",
  target_code: "halo_halo",
  target_name: "Halo Halo",
  type: "function",
};

const bodyUpdate = {
  requester: "LNK000632",
  access_target: [
    { role_code: "0008", role_name: "SPG", default_target: 500 },
    { role_code: "0007", role_name: "SPG SELL OUT", default_target: 50 },
    { role_code: "0006", role_name: "SALES TO", default_target: 100 },
  ],
  segment_code: "0001",
  segment_name: "Retail",
  target_code: "halo_halo",
  target_name: "Halo Halo",
  type: "menu",
  menu_code: "check_out",
  menu_name: "Check Out",
};

module.exports = {
  schemaTarget,
  bodyCreate,
  bodyUpdate,
};
