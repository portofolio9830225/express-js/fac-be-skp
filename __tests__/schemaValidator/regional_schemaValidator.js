require("dotenv").config();
const schemaRegional = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    regional_code: {
      type: "string",
    },
    regional_name: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateRegional = {
  requester: "LNK000632",
  regional_name: "Other Island",
};

const bodyUpdateRegional = {
  _id: "",
  requester: "LNK000632",
  regional_name: "Other Island New",
};

module.exports = {
  schemaRegional,
  bodyCreateRegional,
  bodyUpdateRegional,
};
