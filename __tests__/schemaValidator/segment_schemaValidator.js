require("dotenv").config();
const schemaSegment = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    company_code: {
      type: "string",
    },
    company_name: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    country_code: {
      type: "string",
    },
    country_name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateSegment = {
  requester: "LNK000632",
  segment_name: "Retail Indonesia",
};

const bodyUpdateSegment = {
  _id: "",
  requester: "LNK000632",
  segment_name: "Retail Indonesia New",
};

module.exports = {
  schemaSegment,
  bodyCreateSegment,
  bodyUpdateSegment,
};
