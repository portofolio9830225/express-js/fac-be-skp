require("dotenv").config();
const schemaCompany = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    code: {
      type: "string",
    },
    name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateCompany = {
  requester: "LNK000632",
  name: "PT. Lautan Natural Krimerindo",
};

module.exports = {
  schemaCompany,
  bodyCreateCompany,
};
