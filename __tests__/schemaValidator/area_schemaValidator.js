require("dotenv").config();
const schemaArea = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    area_code: {
      type: "string",
    },
    area_name: {
      type: "string",
    },
    regional_code: {
      type: "string",
    },
    regional_name: {
      type: "string",
    },
    city: {
      type: "array",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateArea = {
  requester: "LNK000632",
  area_name: "AREA TEST 1",
  city: [
    {
      value: "12",
      label: "(12) KOTA TEST",
    },
  ],
};

const bodyUpdateArea = {
  _id: "",
  requester: "LNK000632",
  area_name: "AREA TEST 1",
  city: [
    {
      value: "12",
      label: "(12) KOTA TEST",
    },
    {
      value: "13",
      label: "(13) KOTA TEST2",
    },
  ],
};

module.exports = {
  schemaArea,
  bodyCreateArea,
  bodyUpdateArea,
};
