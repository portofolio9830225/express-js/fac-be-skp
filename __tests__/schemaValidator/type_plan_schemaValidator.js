const schemaTypePlan = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    code: {
      type: "string",
    },
    name: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
    is_showing: {
      type: "boolean",
    },
  },
};

const bodyCreateTypePlan = {
  requester: "LNK000632",
  code: "Event",
  name: "Event",
  segment_code: "0001",
  segment_name: "Retail",
  store_category_code: "nasional",
  store_category_name: "Nasional",
  is_store_required: true,
  is_showing: true,
};

const bodyUpdateTypePlan = {
  requester: "LNK000632",
  code: "VisitPlan",
  name: "Visit Plan",
  segment_code: "0001",
  segment_name: "Retail",
  store_category_code: "nasional",
  store_category_name: "Nasional",
  is_store_required: true,
  is_showing: true,
};

module.exports = {
  schemaTypePlan,
  bodyCreateTypePlan,
  bodyUpdateTypePlan,
};
