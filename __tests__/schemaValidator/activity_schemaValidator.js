require("dotenv").config();
const schemaActivity = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    segment: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    menu_code: {
      type: "string",
    },
    menu_name: {
      type: "string",
    },
    type_code: {
      type: "string",
    },
    type_name: {
      type: "string",
    },
    short_category_code: {
      type: "string",
    },
    short_category_name: {
      type: "string",
    },
    is_mandatory: {
      type: "boolean",
    },
    access_after: {
      type: "object",
    },
    ordering: {
      type: "number",
    },
    role: {
      type: "string",
    },
    role_code: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateActivity = {
  requester: "LNK000632",
  menu_name: "TEST 1",
  menu_code: "TEST_1",
  type_code: "Event",
  type_name: "Event",
  store_category_code: "nasional",
  store_category_name: "Nasional",
  is_mandatory: true,
  access_after: {},
  segment: "Retail",
  segment_code: "0001",
  type: "function",
  ordering: 1,
  role: "Sales",
  role_code: "0001",
  field: [
    {
      id: 0,
      field_name: "field 1",
      field_code: "field_1",
      is_mandatory: true,
      ordering: "2",
    },
  ],
};

const bodyUpdateActivity = {
  requester: "LNK000632",
  menu_name: "TEST 1",
  menu_code: "TEST_1",
  type_code: "Event",
  type_name: "Event",
  store_category_code: "nasional",
  store_category_name: "Nasional",
  is_mandatory: true,
  access_after: {},
  segment: "Retail",
  segment_code: "0001",
  type: "function",
  ordering: 1,
  role: "Sales",
  role_code: "0001",
  field: [
    {
      id: 0,
      field_name: "field 1",
      field_code: "field_1",
      is_mandatory: true,
      ordering: "2",
    },
  ],
};

module.exports = {
  schemaActivity,
  bodyCreateActivity,
  bodyUpdateActivity,
};
