require("dotenv").config();
const schemaFieldInput = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    field_code: {
      type: "string",
    },
    field_name: {
      type: "string",
    },
    tipe_data: {
      type: "string",
    },
    category: {
      type: "string",
    },
    segment: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    accepted: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateFieldInput = {
  requester: "LNK000784",
  field_name: "field 1",
  field_code: "field_1",
  tipe_data: "text",
  category: "input",
  segment: "Retail",
  segment_code: "0001",
};

const bodyUpdateFieldInput = {
  execute_date: "2022-07-14",
  requester: "LNK000784",
  field_name: "field 1",
  field_code: "field_1",
  tipe_data: "text",
  category: "single_select",
  segment: "Retail",
  segment_code: "0001",
  accepted: {
    value: "a",
    label: "a",
  },
};

module.exports = {
  schemaFieldInput,
  bodyCreateFieldInput,
  bodyUpdateFieldInput,
};
