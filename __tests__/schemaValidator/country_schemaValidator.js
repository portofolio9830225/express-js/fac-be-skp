require("dotenv").config();
const schemaCountry = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    country_code: {
      type: "string",
    },
    country_iso_code: {
      type: "string",
    },
    country_name: {
      type: "string",
    },
    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateCountry = {
  requester: "LNK000632",
  country_code: "62",
  country_iso_code: "ID",
  country_name: "IDONESIA",
};

module.exports = {
  schemaCountry,
  bodyCreateCountry,
};
