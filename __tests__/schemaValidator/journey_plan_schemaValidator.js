require("dotenv").config();
const schemaArea = {
  type: "object",
  properties: {
    _id: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    segment_code: {
      type: "string",
    },
    segment_name: {
      type: "string",
    },
    area_code: {
      type: "string",
    },
    area_name: {
      type: "string",
    },
    regional_code: {
      type: "string",
    },
    regional_name: {
      type: "string",
    },
    city_name: {
      type: "string",
    },
    city_code: {
      type: "string",
    },
    date: {
      type: "string",
    },
    iso_date: {
      type: "date",
    },
    username: {
      type: "string",
    },
    name: {
      type: "string",
    },
    type_code: {
      type: "string",
    },
    type_name: {
      type: "string",
    },
    roles: {
      type: "string",
    },
    requester: {
      type: "string",
    },
    requester_name: {
      type: "string",
    },
    status: {
      type: "string",
    },
    remark: {
      type: "string",
    },
    store_code: {
      type: "string",
    },
    store_name: {
      type: "string",
    },
    store: {
      type: "object",
    },

    created_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    deleted_at: {
      type: "string",
      pattern: "^(.*)$",
    },
    updated_by: {
      type: "string",
    },
    deleted_by: {
      type: "string",
    },
    is_active: {
      type: "boolean",
    },
  },
};

const bodyCreateJourneyPlan = {
  requester: "LNK000632",
  area_name: "AREA TEST 1",
  city: [
    {
      value: "12",
      label: "(12) KOTA TEST",
    },
  ],
};

module.exports = {
  schemaJourneyPlan,
  bodyCreateJourneyPlan,
};
