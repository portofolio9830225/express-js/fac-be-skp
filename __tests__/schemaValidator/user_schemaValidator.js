require("dotenv").config();
const schemaUser = {
  type: "object",
  properties: {
    _id: { type: "string" },
    is_active: { type: "boolean" },
    requester: { type: "string" },
    role_code: { type: "string" },
    role_name: { type: "string" },
    segment_code: { type: "string" },
    segment_name: { type: "string" },
    created_at: { type: "string" },
    updated_at: { type: "string" },
    updated_by: { type: "string" },
    deleted_at: { type: "string" },
    deleted_by: { type: "string" },
  },
};

const bodyUser = {
  requester: "LNK000632",
  username: "TEST10001",
  name: "Subaktiyar",
  email: "mis.stf11@lnk.co.id",
  detail_user: { phone: "0850000001" },
  type: "guest",
  department: "guest",
  role: [
    {
      role_code: "0002",
      role_name: "ASS",
      segment_code: "0001",
      segment_name: "Retail",
      is_default: true,
      parent_code: "LNK000745",
      parent_name: "Octavian Ardy Bagus P",
      regional: [{ regional_code: "0001-002", regional_name: "East" }],
    },
  ],
  segment_code: "0001",
};

const bodyUserUpdate = {
  requester: "LNK000632",
  username: "TEST10001",
  name: "Subaktiyar",
  email: "mis.stf11@lnk.co.id",
  detail_user: { phone: "0851111111" },
  type: "guest",
  department: "guest",
  role: [
    {
      role_code: "0002",
      role_name: "ASS",
      segment_code: "0001",
      segment_name: "Retail",
      is_default: true,
      parent_code: "LNK000745",
      parent_name: "Octavian Ardy Bagus P",
      regional: [{ regional_code: "0001-001", regional_name: "West" }],
    },
  ],
  segment_code: "0001",
};

module.exports = {
  schemaUser,
  bodyUser,
  bodyUserUpdate,
};
