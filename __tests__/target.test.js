const env = require("dotenv").config();
process.env.NODE_ENV = "test";

const {
  bodyCreate,
  bodyUpdate,
  schemaTarget,
} = require("./schemaValidator/target_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:target-test");
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.target.deleteMany();
  await db.target_history.deleteMany();
});

afterAll((done) => {
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE TARGET", () => {
  it("POST /api/target", async () => {
    const { requester, segment_code } = bodyCreate;

    // create
    const createTarget = await reqWithAuth("/api/target", "post", bodyCreate);
    expect(createTarget.status).toEqual(200);
    expect(createTarget.body).toMatchSchema(schemaTarget);

    // read
    const readTarget = await reqWithAuth(
      "/api/target/data-target?segment_code=" + segment_code,
      "get"
    );
    expect(readTarget.status).toEqual(200);
    expect(readTarget.body).toMatchSchema(schemaTarget);

    // update
    const updateTarget = await reqWithAuth("/api/target", "put", {
      ...bodyUpdate,
      _id: readTarget.body.foundData[0]._id,
      requester,
    });
    expect(updateTarget.status).toEqual(200);
    expect(updateTarget.body).toMatchSchema(schemaTarget);

    // delete
    const deleteTarget = await reqWithAuth("/api/target", "delete", {
      _id: readTarget.body.foundData[0]._id,
      requester,
    });
    expect(deleteTarget.status).toEqual(200);
    expect(deleteTarget.body).toMatchSchema(schemaTarget);
  });
});

describe("CREATE, READ, UPDATE, DELETE TARGET UNEXPECTED", () => {
  it("POST /api/target", async () => {
    const { requester, segment_code } = bodyCreate;

    // create
    const createTarget = await reqWithAuth("/api/target", "post", {});
    expect(createTarget.status).toEqual(400);
    expect(createTarget.body).toMatchSchema(schemaTarget);

    // read
    const readTarget = await reqWithAuth(
      "/api/target/data-target?segment_code=",
      "get"
    );
    expect(readTarget.status).toEqual(400);
    expect(readTarget.body).toMatchSchema(schemaTarget);

    // update
    const updateTarget = await reqWithAuth("/api/target", "put", {
      ...bodyUpdate,
      requester,
    });
    expect(updateTarget.status).toEqual(400);
    expect(updateTarget.body).toMatchSchema(schemaTarget);

    // delete
    const deleteTarget = await reqWithAuth("/api/target", "delete", {
      requester,
    });
    expect(deleteTarget.status).toEqual(400);
    expect(deleteTarget.body).toMatchSchema(schemaTarget);
  });
});
