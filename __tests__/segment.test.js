const env = require("dotenv").config();
process.env.NODE_ENV = "test";
const {
  schemaCompany,
  bodyCreateCompany,
} = require("./schemaValidator/company_schemaValidator");
const {
  schemaCountry,
  bodyCreateCountry,
} = require("./schemaValidator/country_schemaValidator");
const {
  schemaSegment,
  bodyCreateSegment,
  bodyUpdateSegment,
} = require("./schemaValidator/segment_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-checklist:template-selection-test"); //jest json schema
const dateFormat = require("dateformat");
const { required } = require("joi");
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.country.deleteMany();
  await db.company.deleteMany();
  await db.segment.deleteMany({});
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE, SEGMENT", () => {
  it("POST /api/segment", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    debug(readSegment.body, "=> readSegment");

    //update segment
    bodyUpdateSegment._id = readSegment.body.foundData[0]._id;
    const updateSegment = await reqWithAuth(
      "/api/segment",
      "put",
      bodyUpdateSegment
    );

    expect(updateSegment.status).toEqual(200);
    expect(updateSegment.body).toMatchSchema(schemaSegment);

    //delete
    let deleteBody = { requester: "LNK000632" };
    deleteBody._id = readSegment.body.foundData[0]._id;
    const DeletedData = await reqWithAuth(
      "/api/segment/",
      "delete",
      deleteBody
    );
    expect(DeletedData.status).toEqual(200);
    expect(DeletedData.body).toMatchSchema(schemaSegment);
  });
});

describe("CREATE, READ, UPDATE, DELETE, SEGMENT UNEXPECTED", () => {
  it("POST /api/segment", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;
    bodyCreateSegment.segment_code = "001";

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(400);
    expect(newData.body).toMatchSchema(schemaSegment);

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    debug(readSegment.body, "=> readSegment");

    //update segment
    bodyUpdateSegment._id = "";
    const updateSegment = await reqWithAuth(
      "/api/segment",
      "put",
      bodyUpdateSegment
    );

    expect(updateSegment.status).toEqual(400);
    expect(updateSegment.body).toMatchSchema(schemaSegment);

    //delete
    let deleteBody = { requester: "LNK000632" };
    deleteBody._id = "";
    const DeletedData = await reqWithAuth(
      "/api/segment/",
      "delete",
      deleteBody
    );
    expect(DeletedData.status).toEqual(400);
    expect(DeletedData.body).toMatchSchema(schemaSegment);
  });
});
