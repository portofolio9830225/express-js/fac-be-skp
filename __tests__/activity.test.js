const env = require("dotenv").config();
process.env.NODE_ENV = "test";
const {
  schemaRoles,
  bodyRoles,
} = require("./schemaValidator/roles_schemaValidator");
const {
  schemaStoreCategory,
  bodyCreateStoreCateogory,
  bodyUpdateStoreCateogory,
} = require("./schemaValidator/store_category_schemaValidator");
const {
  schemaTypePlan,
  bodyCreateTypePlan,
  bodyUpdateTypePlan,
} = require("./schemaValidator/type_plan_schemaValidator");
const {
  schemaCompany,
  bodyCreateCompany,
} = require("./schemaValidator/company_schemaValidator");
const {
  schemaCountry,
  bodyCreateCountry,
} = require("./schemaValidator/country_schemaValidator");

const {
  bodyCreateFieldInput,
  bodyUpdateFieldInput,
  schemaFieldInput,
} = require("./schemaValidator/field_input_schemaValidator");
const {
  schemaSegment,
  bodyCreateSegment,
  bodyUpdateSegment,
} = require("./schemaValidator/segment_schemaValidator");

const {
  schemaActivity,
  bodyCreateActivity,
  bodyUpdateActivity,
} = require("./schemaValidator/activity_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:activity-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);
const moment = require("moment");

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.field_input.deleteMany();
  await db.queue_update.deleteMany();
  await db.roles.deleteMany();
  await db.segment.deleteMany();
  await db.activity.deleteMany();
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE, ACTIVITY", () => {
  it("POST /api/activity", async () => {
    // create role
    const createRole = await reqWithAuth("/api/roles", "post", bodyRoles);
    debug(createRole, "-> createRole");

    expect(createRole.status).toEqual(200);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // create store category
    const createStoreCategory = await reqWithAuth(
      "/api/store_category/",
      "post",
      bodyCreateStoreCateogory
    );
    expect(createStoreCategory.status).toEqual(200);
    expect(createStoreCategory.body).toMatchSchema(schemaStoreCategory);

    // create type plan
    const createTypePlan = await reqWithAuth(
      "/api/type_plan/",
      "post",
      bodyCreateTypePlan
    );
    expect(createTypePlan.status).toEqual(200);
    expect(createTypePlan.body).toMatchSchema(schemaTypePlan);

    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    // create
    const createFieldInput = await reqWithAuth(
      "/api/field_input",
      "post",
      bodyCreateFieldInput
    );
    expect(createFieldInput.status).toEqual(200);
    expect(createFieldInput.body).toMatchSchema(schemaFieldInput);

    // read
    const readFieldInput = await reqWithAuth(
      "/api/field_input/?segment_code=0001",
      "get"
    );
    expect(readFieldInput.status).toEqual(200);
    expect(readFieldInput.body).toMatchSchema(schemaFieldInput);

    // create
    const createActivity = await reqWithAuth(
      "/api/activity",
      "post",
      bodyCreateActivity
    );

    debug(createActivity, "-->> createActivity");
    expect(createActivity.status).toEqual(200);
    expect(createActivity.body).toMatchSchema(schemaActivity);

    // read
    const readActivity = await reqWithAuth(
      "/api/activity/?segment_code=0001",
      "get"
    );
    expect(readActivity.status).toEqual(200);
    expect(readActivity.body).toMatchSchema(schemaActivity);

    debug(readActivity.body, "=> readActivity");

    bodyUpdateActivity._id = readActivity.body.foundData[0]._id;
    bodyUpdateActivity.execute_date = moment().format("YYYY-MM-DD");
    //update
    const updateActivity = await reqWithAuth(
      "/api/activity/",
      "put",
      bodyUpdateActivity
    );

    debug(updateActivity, "--->>> updateActivity");
    expect(updateActivity.status).toEqual(200);
    expect(updateActivity.body).toMatchSchema(schemaActivity);

    //delete
    const deleteBody = { requester: "LNK000632" };
    deleteBody._id = readActivity.body.foundData[0]._id;
    const deleteActivity = await reqWithAuth(
      "/api/activity/",
      "delete",
      deleteBody
    );
    expect(deleteActivity.status).toEqual(200);
    expect(deleteActivity.body).toMatchSchema(schemaActivity);
  });
});

describe("CREATE, READ, UPDATE, DELETE, ACTIVITY UNEXPECTED", () => {
  it("POST /api/activity", async () => {
    // create role
    const createRole = await reqWithAuth("/api/roles", "post", bodyRoles);
    debug(createRole, "-> createRole");

    expect(createRole.status).toEqual(200);
    expect(createRole.body).toMatchSchema(schemaRoles);

    // create store category
    const createStoreCategory = await reqWithAuth(
      "/api/store_category/",
      "post",
      bodyCreateStoreCateogory
    );
    expect(createStoreCategory.status).toEqual(200);
    expect(createStoreCategory.body).toMatchSchema(schemaStoreCategory);

    // create type plan
    const createTypePlan = await reqWithAuth(
      "/api/type_plan/",
      "post",
      bodyCreateTypePlan
    );
    expect(createTypePlan.status).toEqual(200);
    expect(createTypePlan.body).toMatchSchema(schemaTypePlan);

    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    debug(readSegment.body, "=> readSegment");
    // create
    const createFieldInput = await reqWithAuth(
      "/api/field_input",
      "post",
      bodyCreateFieldInput
    );
    expect(createFieldInput.status).toEqual(200);
    expect(createFieldInput.body).toMatchSchema(schemaFieldInput);

    // read
    const readFieldInput = await reqWithAuth(
      "/api/field_input/?segment_code=0001",
      "get"
    );
    expect(readFieldInput.status).toEqual(200);
    expect(readFieldInput.body).toMatchSchema(schemaFieldInput);

    debug(readFieldInput.body, "=> readFieldInput");

    // create
    const createActivity = await reqWithAuth(
      "/api/activity",
      "post",
      bodyCreateActivity
    );

    debug(bodyCreateActivity, "+>> body create actv");

    debug(createActivity, "-=>> createActivity");
    expect(createActivity.status).toEqual(200);
    expect(createActivity.body).toMatchSchema(schemaActivity);

    // read
    const readActivity = await reqWithAuth(
      "/api/activity/?segmen_code=0002",
      "get"
    );
    expect(readActivity.status).toEqual(200);
    expect(readActivity.body).toMatchSchema(schemaActivity);

    debug(readActivity.body, "=> readActivity");

    //update
    const updateActivity = await reqWithAuth(
      "/api/activit/",
      "put",
      bodyUpdateActivity
    );
    expect(updateActivity.status).toEqual(404);
    expect(updateActivity.body).toMatchSchema(schemaActivity);

    //delete
    const deleteBody = { requester: "LNK000632" };
    deleteBody._id = readActivity.body.foundData[0]._id;
    const deleteActivity = await reqWithAuth(
      "/api/activity/",
      "delete",
      deleteBody
    );
    expect(deleteActivity.status).toEqual(200);
    expect(deleteActivity.body).toMatchSchema(schemaActivity);
  });
});
