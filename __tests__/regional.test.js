const env = require("dotenv").config();
process.env.NODE_ENV = "test";
const {
  schemaCompany,
  bodyCreateCompany,
} = require("./schemaValidator/company_schemaValidator");
const {
  schemaCountry,
  bodyCreateCountry,
} = require("./schemaValidator/country_schemaValidator");
const {
  schemaSegment,
  bodyCreateSegment,
  bodyUpdateSegment,
} = require("./schemaValidator/segment_schemaValidator");
const {
  schemaRegional,
  bodyCreateRegional,
  bodyUpdateRegional,
} = require("./schemaValidator/regional_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:regional-test"); //jest json schema
const dateFormat = require("dateformat");
const { required } = require("joi");
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.country.deleteMany();
  await db.company.deleteMany();
  await db.segment.deleteMany({});
  await db.regional.deleteMany({});
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE REGIONAL", () => {
  it("POST /api/regional", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    //create regional
    bodyCreateRegional.segment_code =
      readSegment.body.foundData[0].segment_code;
    const newDataRegional = await reqWithAuth(
      "/api/regional",
      "post",
      bodyCreateRegional
    );
    expect(newDataRegional.status).toEqual(200);
    expect(newDataRegional.body).toMatchSchema(schemaRegional);
    expect(newDataRegional.body).toHaveProperty("_id");

    //read regional
    const readRegional = await reqWithAuth(
      `/api/regional/?page=1&sizePerPage=10&segment_code=${readSegment.body.foundData[0].segment_code}`,
      "get"
    );
    expect(readRegional.status).toEqual(200);
    expect(readRegional.body).toMatchSchema(schemaRegional);

    //update regional
    bodyUpdateRegional._id = readRegional.body.foundData[0]._id;
    const updateRegional = await reqWithAuth(
      "/api/regional",
      "put",
      bodyUpdateRegional
    );
    expect(updateRegional.status).toEqual(200);
    expect(updateRegional.body).toMatchSchema(schemaRegional);

    //delete regional
    let deleteBody = { requester: "LNK000632" };
    deleteBody._id = readRegional.body.foundData[0]._id;
    const DeletedDataRegional = await reqWithAuth(
      "/api/regional/",
      "delete",
      deleteBody
    );
    expect(DeletedDataRegional.status).toEqual(200);
    expect(DeletedDataRegional.body).toMatchSchema(schemaRegional);
  });
});

describe("CREATE, READ, UPDATE, DELETE REGIONAL UNEXPECTED", () => {
  it("POST /api/regional", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;
    bodyCreateSegment.segment_code = "001";

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(400);
    expect(newData.body).toMatchSchema(schemaSegment);

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    //create regional
    bodyCreateRegional.segment_code = "";
    const newDataRegional = await reqWithAuth(
      "/api/regional",
      "post",
      bodyCreateRegional
    );
    expect(newDataRegional.status).toEqual(400);
    expect(newDataRegional.body).toMatchSchema(schemaRegional);

    //read regional
    const readRegional = await reqWithAuth(
      "/api/regional/?page=1&sizePerPage=10",
      "get"
    );
    expect(readRegional.status).toEqual(200);
    expect(readRegional.body).toMatchSchema(schemaRegional);

    //update regional
    bodyUpdateRegional._id = "";
    const updateRegional = await reqWithAuth(
      "/api/regional",
      "put",
      bodyUpdateRegional
    );
    expect(updateRegional.status).toEqual(400);
    expect(updateRegional.body).toMatchSchema(schemaRegional);

    //delete regional
    let deleteBody = { requester: "LNK000632" };
    deleteBody._id = "";
    const DeletedDataRegional = await reqWithAuth(
      "/api/regional/",
      "delete",
      deleteBody
    );
    expect(DeletedDataRegional.status).toEqual(400);
    expect(DeletedDataRegional.body).toMatchSchema(schemaRegional);
  });
});
