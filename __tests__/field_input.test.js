const env = require("dotenv").config();
process.env.NODE_ENV = "test";
const {
  schemaCompany,
  bodyCreateCompany,
} = require("./schemaValidator/company_schemaValidator");
const {
  schemaCountry,
  bodyCreateCountry,
} = require("./schemaValidator/country_schemaValidator");

const {
  bodyCreateFieldInput,
  bodyUpdateFieldInput,
  schemaFieldInput,
} = require("./schemaValidator/field_input_schemaValidator");
const {
  schemaSegment,
  bodyCreateSegment,
  bodyUpdateSegment,
} = require("./schemaValidator/segment_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:field-input-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.field_input.deleteMany();
  await db.queue_update.deleteMany();
  await db.country.deleteMany();
  await db.company.deleteMany();
  await db.segment.deleteMany({});
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE, ROLE", () => {
  it("POST /api/field_input", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    debug(readSegment.body, "=> readSegment");
    // create
    const createFieldInput = await reqWithAuth(
      "/api/field_input",
      "post",
      bodyCreateFieldInput
    );
    expect(createFieldInput.status).toEqual(200);
    expect(createFieldInput.body).toMatchSchema(schemaFieldInput);

    // read
    const readFieldInput = await reqWithAuth(
      "/api/field_input/?segment_code=0001",
      "get"
    );
    expect(readFieldInput.status).toEqual(200);
    expect(readFieldInput.body).toMatchSchema(schemaFieldInput);

    debug(readFieldInput.body, "=> readFieldInput");

    // update
    const updateFieldInput = await reqWithAuth("/api/field_input", "put", {
      ...bodyUpdateFieldInput,
      _id: readFieldInput.body.foundData[0]._id,
    });
    expect(updateFieldInput.status).toEqual(200);
    expect(updateFieldInput.body).toMatchSchema(schemaFieldInput);

    // delete
    const deleteFieldInput = await reqWithAuth("/api/field_input", "delete", {
      _id: readFieldInput.body.foundData[0]._id,
      requester: readFieldInput.body.foundData[0].requester,
    });
    expect(deleteFieldInput.status).toEqual(200);
    expect(deleteFieldInput.body).toMatchSchema(schemaFieldInput);
  });
});

describe("CREATE, READ, UPDATE, DELETE, ROLE, UNEXPECTED", () => {
  it("POST /api/field_input", async () => {
    //create Company
    const createCompany = await reqWithAuth(
      "/api/company/",
      "post",
      bodyCreateCompany
    );
    expect(createCompany.status).toEqual(200);
    expect(createCompany.body).toMatchSchema(schemaCompany);

    //read Coampany
    const readCompany = await reqWithAuth(
      "/api/company/?sizePerPage=10&page=1",
      "get"
    );
    expect(readCompany.status).toEqual(200);
    expect(readCompany.body).toMatchSchema(schemaCompany);

    //create Country
    const CreateCountry = await reqWithAuth(
      "/api/country",
      "post",
      bodyCreateCountry
    );
    expect(CreateCountry.status).toEqual(200);
    expect(CreateCountry.body).toMatchSchema(schemaCountry);

    //read parameter checklist
    const readCountry = await reqWithAuth("/api/country/", "get");
    expect(readCountry.status).toEqual(200);
    expect(readCountry.body).toMatchSchema(schemaCountry);

    //create segment
    bodyCreateSegment.company_code = readCompany.body.foundData[0].code;
    bodyCreateSegment.country_code = readCountry.body.foundData[0].country_code;

    const newData = await reqWithAuth(
      "/api/segment",
      "post",
      bodyCreateSegment
    );
    expect(newData.status).toEqual(200);
    expect(newData.body).toMatchSchema(schemaSegment);
    expect(newData.body).toHaveProperty("_id");

    //read segment
    const readSegment = await reqWithAuth("/api/segment/", "get");
    expect(readSegment.status).toEqual(200);
    expect(readSegment.body).toMatchSchema(schemaSegment);

    debug(readSegment.body, "=> readSegment");
    // create
    const createFieldInput = await reqWithAuth(
      "/api/field_input",
      "post",
      bodyCreateFieldInput
    );
    expect(createFieldInput.status).toEqual(200);
    expect(createFieldInput.body).toMatchSchema(schemaFieldInput);

    // read
    const readFieldInput = await reqWithAuth(
      "/api/field_input/?segment_code=0001",
      "get"
    );
    expect(readFieldInput.status).toEqual(200);
    expect(readFieldInput.body).toMatchSchema(schemaFieldInput);

    // update
    const updateFieldInput = await reqWithAuth("/api/field_input", "put", {
      ...bodyUpdateFieldInput,
      _id: "",
    });
    expect(updateFieldInput.status).toEqual(400);
    expect(updateFieldInput.body).toMatchSchema(schemaFieldInput);

    // delete
    const deleteFieldInput = await reqWithAuth("/api/field_input", "delete", {
      _id: "",
      requester: readFieldInput.body.foundData[0].requester,
    });
    expect(deleteFieldInput.status).toEqual(400);
    expect(deleteFieldInput.body).toMatchSchema(schemaFieldInput);
  });
});
