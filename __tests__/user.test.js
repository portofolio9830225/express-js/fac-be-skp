const env = require("dotenv").config();
process.env.NODE_ENV = "test";

const {
  bodyUser,
  bodyUserUpdate,
  schemaUser,
} = require("./schemaValidator/user_schemaValidator");

const db = require("../models");
const { reqWithAuth } = require("./helper/auth");

const debug = require("debug")("backend-fibersquad:user-test"); //jest json schema
const matchers = require("jest-json-schema").matchers;
expect.extend(matchers);

beforeAll((done) => {
  done();
});

afterEach(async () => {
  await db.authentication.deleteMany();
  await db.authentication_history.deleteMany();
});
afterAll((done) => {
  // Closing the DB connection allows Jest to exit successfully.
  db.mongoose.connection.close();
  done();
});

describe("CREATE, READ, UPDATE, DELETE USER", () => {
  it("POST /api/user", async () => {
    const { requester } = bodyUser;
    const segment_code = "0001";

    // create
    const createUser = await reqWithAuth("/api/user", "post", bodyUser);
    expect(createUser.status).toEqual(200);
    expect(createUser.body).toMatchSchema(schemaUser);

    // read
    const readUser = await reqWithAuth(
      `/api/user?segment_code=${segment_code}&show_all=true`,
      "get"
    );
    expect(readUser.status).toEqual(200);
    expect(readUser.body).toMatchSchema(schemaUser);

    // update
    const updateUser = await reqWithAuth("/api/user", "put", {
      ...bodyUserUpdate,
      _id: readUser.body.foundData[0]._id,
    });
    expect(updateUser.status).toEqual(200);
    expect(updateUser.body).toMatchSchema(schemaUser);

    // delete
    const deleteUser = await reqWithAuth("/api/user", "delete", {
      _id: readUser.body.foundData[0]._id,
      requester,
    });
    expect(deleteUser.status).toEqual(200);
    expect(deleteUser.body).toMatchSchema(schemaUser);
  });
});

describe("CREATE, READ, UPDATE, DELETE USER UNEXPECTED", () => {
  it("POST /api/user", async () => {
    const segment_code = "";
    const requester = "";

    // create
    const createUser = await reqWithAuth("/api/user", "post", {});
    expect(createUser.status).toEqual(400);
    expect(createUser.body).toMatchSchema(schemaUser);

    // read
    const readUser = await reqWithAuth(
      `/api/user?segment_code=${segment_code}&show_all=true`,
      "get"
    );
    expect(readUser.status).toEqual(400);
    expect(readUser.body).toMatchSchema(schemaUser);

    // update
    const updateUser = await reqWithAuth("/api/user", "put", {
      ...bodyUserUpdate,
      _id: "",
    });
    expect(updateUser.status).toEqual(400);
    expect(updateUser.body).toMatchSchema(schemaUser);

    // delete
    const deleteUser = await reqWithAuth("/api/user", "delete", {
      _id: "",
      requester,
    });
    expect(deleteUser.status).toEqual(400);
    expect(deleteUser.body).toMatchSchema(schemaUser);
  });
});
