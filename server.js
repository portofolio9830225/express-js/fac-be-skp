require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const fs = require("fs");
const helmet = require("helmet");
const debug = require("debug")("backend-skp-tt:server");
const error = require("./common/errorMessage");
const rfs = require("rotating-file-stream");
const app = express();
const cors = require("cors");
const cron = require("node-cron");

app.use(cors());

if (process.env.NODE_ENV === "production") {
	const accessLogStream = rfs.createStream("server.log", {
		interval: "1d", // rotate daily
		path: path.join(__dirname, "log"),
	});
	app.use(logger("combined", { stream: accessLogStream }));
} else {
	app.use(logger("combined"));
}
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "public")));

const db = require("./models");
let MONGO_URI = process.env.NODE_ENV == "test" ? process.env.MONGO_CLOUD_TEST : process.env.MONGO_CLOUD;

debug(`will connect to : ${MONGO_URI}`);
db.mongoose
	.connect(`${MONGO_URI}`, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		auth: { authSource: "admin" },
		user: `${process.env.MONGO_USERNAME}`,
		pass: `${process.env.MONGO_PASSWORD}`,
	})

	.then(() => {
		debug("Successfully connect to MongoDB.");
	})
	.catch(err => {
		debug("Connection error", err);
		process.exit();
	});
const arrayAllowEnv = ["production", "staging", "development"];
if (arrayAllowEnv.includes(process.env.NODE_ENV)) {
	//cron
}
//checking health pod
app.get("/health", (req, res) => {
	res.send(`Apps is healthy`);
});
app.get("/", (req, res) => {
	res.send(`Welcome to Backend HRIS`);
});

const authRouter = require("./routes/auth");
app.use("/api/auth", authRouter);
const menuRouter = require("./routes/menu");
app.use("/api/menu", menuRouter);
const submenuRouter = require("./routes/submenu");
app.use("/api/submenu", submenuRouter);
const authorizationRouter = require("./routes/authorization");
app.use("/api/authorization", authorizationRouter);

const masterPointRouter = require("./routes/rizky/masterPoint");
app.use("/api/master-points", masterPointRouter);
const masterNkaRouter = require("./routes/rizky/masterNkaRoute");
app.use("/api/master-nka", masterNkaRouter);
const tradingTermPointsRoute = require("./routes/rizky/trading-term-points.Route");
app.use("/api/trading-term-points", tradingTermPointsRoute);
const skpRoute = require("./routes/rizky/skpRoute");
app.use("/api/skp", skpRoute);
const productsRoute = require("./routes/rizky/productRoute");
app.use("/api/products", productsRoute);
const claimsRoute = require("./routes/rizky/claimRoute");
app.use("/api/claims", claimsRoute);
const parametersRoute = require("./routes/rizky/parametersRoute");
app.use("/api/parameters", parametersRoute);
const reportRoute = require("./routes/rizky/reportRoute");
app.use("/api/report-expsense", reportRoute);

const userRouter = require("./routes/user");
app.use("/api/user", userRouter);

const organizationRouter = require("./routes/organizations");
app.use("/api/user", organizationRouter);
const realisationRouter = require("./routes/realisation");
app.use("/api/realisation", realisationRouter);
const reportRouter = require("./routes/report");
app.use("/api/report", reportRouter);

const summaryAccount = require("./routes/sodiq/summary-accountRoute");
app.use("/api/parameter/", summaryAccount);

const budgetTargetRouter = require("./routes/sodiq/budget-target.Route");
app.use("/api/budget-target", budgetTargetRouter);

const brandRouter = require("./routes/handri/brandRoute");
app.use("/api/brand", brandRouter);

app.use((req, res, next) => {
	return res.status(404).send(error.errorReturn({ message: "Invalid routes!" }));
});
module.exports = app;
