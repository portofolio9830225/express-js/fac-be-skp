const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;

db.set_active_log = require("./set_active_log.model");
db.authorization = require("./authorization.model");
db.menu = require("./menu.model");
db.submenu = require("./submenu.model");
db.user = require("./user.model");
db.authentication = require("./authentication.model");
db.authentication_history = require("./authentication_history.model");
db.file = require("./file.model");
db.role = require("./roles.model");
db.master_points = require("./rizky/master-points.model");
db.masrer_nka = require("./rizky/master_nka.model");
db.brand = require("./handri/brand.model");
db.trading_term_points = require("./rizky/trading_term_points.model");
db.skp = require("./rizky/skp.model");
db.products = require("./rizky/product.model");
db.budget_target = require("./sodiq/budget_target.model");
db.claims = require("./rizky/claim.model");
db.claim_F = require("./faiz/claim.model");
db.dummy_claim = require("./handri/dummy_claim.model");
db.dummy_parameter = require("./handri/dummy_parameter.model");

db.parameter = require("./parameter.model");

db.organizations = require("./organization.model");

module.exports = db;
