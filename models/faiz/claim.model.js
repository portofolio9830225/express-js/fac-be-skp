const mongoose = require("mongoose");
const Claim_F = mongoose.model(
  "Claims",
  new mongoose.Schema({
    is_active: { type: Boolean, required: true },
    account_name: { type: String, required: true },
    account_code: { type: String, required: true },
    periode_claim: { type: String, required: true },
    promo_type: { type: String },
    amount: { type: Number, required: true },
    remark: { type: String },
    type: { type: String, required: true },
    skp: { type: Object },
    created_at: { type: Date },
  }),
  "claims"
);
module.exports = Claim_F;
