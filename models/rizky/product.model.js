const { toArray } = require("lodash");
const mongoose = require("mongoose");
const Product = mongoose.model(
    "Product",
    new mongoose.Schema({
        is_active: { type: Boolean },
        product_name: { type: String },
        product_code: { type: String }
    }),
    "products"
);
module.exports = Product;