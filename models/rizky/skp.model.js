const { toArray } = require("lodash");
const mongoose = require("mongoose");
const Skp = mongoose.model(
    "Skp",
    new mongoose.Schema({
        is_active: { type: Boolean },
        skp_number: { type: String },
        program_name: { type: String },
        mechanism: { type: String },
        program_start: { type: Date },
        program_end: { type: Date },
        product: { type: Array },
        amount: { type: Number },
        account_name: { type: String },
        account_code: { type: String },
        optional_parameter: { type: Object },
        created_at: { type: Date },
    }),
    "skp"
);
module.exports = Skp;