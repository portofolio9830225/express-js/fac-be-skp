const { toArray } = require("lodash");
const mongoose = require("mongoose");
const TradingTermPoints = mongoose.model(
    "Trading Term Points",
    new mongoose.Schema({
        is_active: { type: Boolean },
        account_name: { type: String },
        account_code: { type: String },
        year: { type: String },
        year_iso: { type: Date },
        created_at: { type: Date },
        point: { type: Object },
    }),
    "trading_term_points"
);
module.exports = TradingTermPoints;