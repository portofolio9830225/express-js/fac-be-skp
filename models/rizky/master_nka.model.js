const { toArray } = require("lodash");
const mongoose = require("mongoose");
const MasterNka = mongoose.model(
    "MasterNka",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String },
        code: { type: String }
    }),
    "master_nka"
);
module.exports = MasterNka;