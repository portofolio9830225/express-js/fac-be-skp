const { toArray } = require("lodash");
const mongoose = require("mongoose");
const MasterPointsModel = mongoose.model(
    "MasterPoint",
    new mongoose.Schema({
        is_active: { type: Boolean },
        name: { type: String }
    }),
    "master_points"
);
module.exports = MasterPointsModel;