const mongoose = require("mongoose");
const Parameter = mongoose.model(
  "Parameter",
  new mongoose.Schema({
    store_name: { type: String, required: true },
    store_code: { type: String, required: true },
    store_group: { type: String },
    store_segment: { type: String },
    year: { type: String, required: true },
    year_iso: { type: Date, required: true },
    type: { type: String, required: true },
    target: { type: Array, required: true },
    actual: { type: Array, required: true },
    is_active: { type: Boolean, required: true },
    created_at: { type: Date, required: true },
    updated_at: { type: Date },
    created_by: { type: String, required: true },
    updated_by: { type: String },
  }),
  "parameters"
);
module.exports = Parameter;
