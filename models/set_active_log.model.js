const mongoose = require("mongoose");
const SetActiveLog = mongoose.model(
  "SetActiveLog",
  new mongoose.Schema({
    is_active: { type: Boolean },
  }),
  "set_active_log"
);
module.exports = SetActiveLog;
