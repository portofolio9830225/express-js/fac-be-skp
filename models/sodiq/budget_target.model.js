const mongoose = require("mongoose");
const BudgetTarget = mongoose.model(
    "BudgetTarget",
    new mongoose.Schema({
        store_name: { type: String },
        store_code: { type: String },
        store_group: { type: String },
        store_segment: { type: String },
        year: { type: String },
        year_iso: { type: Date },
        is_active: { type: Boolean },
        type: { type: String },
        target: { type: Array },
        actual: { type: Array }
    }),
    "parameters"
);
module.exports = BudgetTarget;