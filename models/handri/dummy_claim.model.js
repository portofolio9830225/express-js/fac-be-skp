const { toArray } = require("lodash");
const mongoose = require("mongoose");
const Claim = mongoose.model(
  "Dummy Claim",
  new mongoose.Schema({
    is_active: { type: Boolean },
    account_name: { type: String },
    account_code: { type: String },
    periode_claim: { type: String },
    promo_type: { type: String },
    amount: { type: Number },
    remark: { type: String },
    type: { type: String },
    skp: { type: Object },
    point_trading_terms: { type: Object },
    created_at: { type: Date },
    distributor: { type: String },
    invoice_number: { type: String },
    branch: { type: String },
    source: { type: String },
    brand: { type: Object },
  }),
  "dummy_claims"
);
module.exports = Claim;
