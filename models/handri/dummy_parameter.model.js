const mongoose = require("mongoose");
const DummyParameter = mongoose.model(
  "Dummy Parameter",
  new mongoose.Schema({
    store_name: { type: String, required: true },
    store_code: { type: String, required: true },
    store_group: { type: String },
    store_segment: { type: String },
    year: { type: String, required: true },
    year_iso: { type: Date, required: true },
    type: { type: String, required: true },
    target: { type: Array, required: true },
    actual: { type: Array, required: true },
    is_active: { type: Boolean, required: true },
  }),
  "dummy_parameters"
);
module.exports = DummyParameter;
