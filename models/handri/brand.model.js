const mongoose = require("mongoose");
const Brand = mongoose.model(
  "Brand",
  new mongoose.Schema({
    is_active: { type: Boolean },
    name: { type: String },
  }),
  "brand"
);
module.exports = Brand;
