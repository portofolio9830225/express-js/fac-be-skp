const mongoose = require("mongoose");
const AuthenticationHistory = mongoose.model(
  "AuthenticationHistory",
  new mongoose.Schema({
    is_active: { type: Boolean },
    department: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, required: true },
    type: { type: String, required: true },
    detail_user: { type: Object, required: true },
    role: { type: Array, required: true },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
    id_history: { type: mongoose.Schema.Types.ObjectId, required: true },
  }),
  "authentications_history"
);
module.exports = AuthenticationHistory;
