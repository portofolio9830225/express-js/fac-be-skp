const mongoose = require("mongoose");
const Authentication = mongoose.model(
  "Authentication",
  new mongoose.Schema({
    is_active: { type: Boolean },
    department: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String },
    last_login: { type: Date },
    fcm_token: { type: String },
    os: { type: String },
    app_version: { type: String },
    forgot_password_token: { type: String },
    forgot_password_expired: { type: String },
    address: { type: String },
    type: { type: String, required: true },
    detail_user: { type: Object, required: true },
    role: { type: Array, required: true },
    created_at: Date,
    created_by: { type: String },
    updated_at: Date,
    updated_by: { type: String },
    deleted_at: Date,
    deleted_by: { type: String },
  }),
  "authentications"
);
module.exports = Authentication;
